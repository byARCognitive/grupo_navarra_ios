using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
public class DownloadObbExample : MonoBehaviour {

    //private string expPath;
    //private bool downloadStarted;

    //void Start() {
    //    expPath = GooglePlayDownloader.GetExpansionFilePath();

    //    if (expPath == null) {
    //        //Error
    //        System.Console.WriteLine("\nOBB DOWNLOAD: External Path not found\n");
    //        return;
    //    } else {
    //        string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
    //        string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);

    //        System.Console.WriteLine("\nOBB DOWNLOAD: External Path: " + expPath + "\n");
    //        System.Console.WriteLine("\nOBB DOWNLOAD: Main Path: " + mainPath + "\n");
    //        System.Console.WriteLine("\nOBB DOWNLOAD: Patch Path: " + mainPath + "\n");

    //        if (mainPath == null) {
    //            System.Console.WriteLine("\nOBB DOWNLOAD: Main was null and OBB will be fetched now\n");

    //            GooglePlayDownloader.FetchOBB();

    //            System.Console.WriteLine("\nOBB DOWNLOAD: OBB has been fetched\n");

    //            StartCoroutine(loadLevel());
    //        } else
    //            SceneManager.LoadScene("SelectIndustrialArquitectSceneV2");
    //           // Application.LoadLevel("02_menues");
    //    }
    //}

    //protected IEnumerator loadLevel() {
    //    string mainPath;

    //    do {
    //        System.Console.WriteLine("\nOBB DOWNLOAD: Trying to fetch main path\n");

    //        yield return new WaitForSeconds(0.5f);
    //        mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);

    //        System.Console.WriteLine("\nOBB DOWNLOAD: Main path = " + mainPath + "\n");

    //    }
    //    while (mainPath == null);

    //    if (downloadStarted == false) {
    //        System.Console.WriteLine("\nOBB DOWNLOAD: Load OBB from mainPath: " + mainPath + "\n");

    //        downloadStarted = true;

    //        string uri = "file://" + mainPath;

    //        WWW www = WWW.LoadFromCacheOrDownload(uri, 0);

    //        // Wait for download to complete
    //        yield return www;

    //        System.Console.WriteLine("\nOBB DOWNLOAD: OBB loaded. Procede to next level...\n");
    //        SceneManager.LoadScene("SelectIndustrialArquitectSceneV2");
    //        //  Application.LoadLevel("02_menues");
    //    }
    //}
   // public GUISkin mySkin;
    private bool obbisok = false;
    private bool replacefiles = false; //true if you bwish to over copy each time
    private string dpack;

    private string filename;
    private WWW unpackerWWW;
    private string[] filesInOBB = {
                                    "VIDEO-NAVARRA_PT.mp4",
                                    "QCAR/NavarraAluminios.dat",
                                    "QCAR/NavarraAluminios.xml"};

    void Update() {
        if (Application.platform == RuntimePlatform.Android) {
            if (Application.dataPath.Contains(".obb") && !obbisok) {
                StartCoroutine(loadOBB());
                obbisok = true;
            }
        } else {
            //SceneManager.LoadScene("AR");
        }



          
    }

    void OnGUI() {
     //   GUI.skin = mySkin;
        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        if (!obbisok) {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUI.contentColor = Color.black;
            GUILayout.Label("There is an installation error with this application, Please re-install!");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    public void StartApp() {
        //loading = true;
        SceneManager.LoadScene("SelectIndustrialArquitectSceneV2");
    }

    public IEnumerator loadOBB() {

        for (int i = 0; i < filesInOBB.Length; ++i) {

            
          

            yield return StartCoroutine(PullStreamingAssetFromObb(filesInOBB[i]));
        }
        yield return new WaitForSeconds(1f);
        StartApp();

    }

    public IEnumerator PullStreamingAssetFromObb(string sapath) {
        if (!File.Exists(Application.persistentDataPath + sapath) || replacefiles) {

        
                 unpackerWWW = new WWW(Application.streamingAssetsPath + "/" + sapath);
      


                

            while (!unpackerWWW.isDone) {
                yield return null;
            }
            if (!string.IsNullOrEmpty(unpackerWWW.error)) {
                dpack = "Error unpacking:" + unpackerWWW.error + " path: " + unpackerWWW.url;
                Debug.Log(dpack);
                yield break; //skip it
            } else {
                dpack = "Extracting " + sapath + " to Persistant Data";
                Debug.Log(dpack);
                if (!Directory.Exists(Path.GetDirectoryName(Application.persistentDataPath + "/" + sapath))) {
                    Directory.CreateDirectory(Path.GetDirectoryName(Application.persistentDataPath + "/" + sapath));
                }
                File.WriteAllBytes(Application.persistentDataPath + "/" + sapath, unpackerWWW.bytes);
                //could add to some kind of uninstall list?
            }
        }
        yield return 0;
    }
}
