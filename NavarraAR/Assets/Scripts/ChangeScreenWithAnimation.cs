﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScreenWithAnimation : MonoBehaviour {

    public Animator anim;
    public string screen;
    public string gameObjectName;
    private string name = "";

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	public void Change() {

        name = gameObject.name;
        anim.SetTrigger("open");
        
	}

    void Update() {


        if (gameObjectName.Equals(name)) {

            if (anim.GetCurrentAnimatorStateInfo(0).IsName("HomeScreenAnimation") && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= .78f) {
                        SceneManager.LoadScene(screen);
            }
        }

         

 

        //    if (anim.GetCurrentAnimatorStateInfo(0).IsName("WhiteScreen")) {
        //    SceneManager.LoadScene(screen);
        //}

    }
     
    
}
