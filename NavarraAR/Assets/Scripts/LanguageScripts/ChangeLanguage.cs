﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using UnityEngine.UI;

public class ChangeLanguage : MonoBehaviour {

    private bool language = false;
    private Lang Lman;
    private Text text;
    private string englishLang = "English";
    private string portugueseLang = "Portuguese";
    private string dataPath;
    private string changeLanguage;   
    private SetUIText[] sendLanguageInfoArray;
    private SpriteState sprState;
    private Button button;

    public static bool dropDownFlag;
    public static bool dropDownCountryFlag;
    public string fileLocation;
    public Sprite PTImage;
    public Sprite UKImage;    

    // Use this for initialization

    void Awake() {

        //if (Application.platform == RuntimePlatform.Android) {
        //       dataPath = "jar:file://" + Application.dataPath + "!/assets/";
        //       dataPath = Application.persistentDataPath + "/Language";
        //    dataPath = Application.dataPath;
        //} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
        //    dataPath = Application.streamingAssetsPath;
        //}
        dropDownFlag = false;
        dropDownCountryFlag = false;

        dataPath = Application.streamingAssetsPath;
        
    }
     
    void Start() {

        button = gameObject.GetComponent<Button>();

        sprState = new SpriteState();

        changeLanguage = PlayerPrefs.GetString("language");
 
        Lman = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation, false);


        if (changeLanguage.Equals(portugueseLang)) { 
            button.image.sprite = PTImage;
            EventManager.Instance.PostNotification(EVENT_TYPE.CHANGE_TO_PT, this);
          
        } else {
            button.image.sprite = UKImage;
            EventManager.Instance.PostNotification(EVENT_TYPE.CHANGE_TO_UK, this);
        
        }
         
    }
      
    public void ChangeLanguages() {

        sendLanguageInfoArray = FindObjectsOfType<SetUIText>();
         
        if (changeLanguage.Equals(portugueseLang)) {
  
            changeLanguage = englishLang;
            Lman.setLanguage(Path.Combine(dataPath, fileLocation), changeLanguage, fileLocation);
            EventManager.Instance.PostNotification(EVENT_TYPE.CHANGE_TO_UK, this);

            button.image.sprite = UKImage; 
                        
        } else {
     
            changeLanguage = portugueseLang;
            Lman.setLanguage(Path.Combine(dataPath, fileLocation), changeLanguage, fileLocation);
            EventManager.Instance.PostNotification(EVENT_TYPE.CHANGE_TO_PT, this);

            button.image.sprite = PTImage;

        }

        ChangeButtonImageLanguage();

        PlayerPrefs.SetString("language", changeLanguage);

        dropDownFlag = true;
        dropDownCountryFlag = true;

        foreach (SetUIText sendLanguageInfo in sendLanguageInfoArray) {
            
            sendLanguageInfo.GetComponent<Text>().text = Lman.getString(sendLanguageInfo.key);

        }
             
    }
    
    private void ChangeButtonImageLanguage() {

    }
}
