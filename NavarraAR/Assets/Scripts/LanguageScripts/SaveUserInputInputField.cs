﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SaveUserInputInputField : MonoBehaviour {

    public string userInputKey;
    public GameObject selectedText;
	// Use this for initialization
	void Start () {
	
	}
	
	public void OnEndEdit() {
        PlayerPrefs.SetString(userInputKey, selectedText.GetComponent<Text>().text);
    }
}
