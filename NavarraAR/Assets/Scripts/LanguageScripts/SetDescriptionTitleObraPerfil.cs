﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetDescriptionTitleObraPerfil : MonoBehaviour {

    public string titleName;

	// Use this for initialization
	void Start () {

        gameObject.GetComponent<Text>().text = PlayerPrefs.GetString(titleName);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
