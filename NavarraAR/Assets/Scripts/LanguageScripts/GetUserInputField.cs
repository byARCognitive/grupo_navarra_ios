﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetUserInputField : MonoBehaviour {

    public string key;
 
	void Start () {

        if (PlayerPrefs.HasKey(key)){
 
             gameObject.GetComponent<InputField>().text = PlayerPrefs.GetString(key);
            
            
        }

	}
	
	 
}
