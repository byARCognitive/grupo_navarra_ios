﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;

public class ChangeBasePointContactos : MonoBehaviour {

    private Toggle myToggle;
    private SetUIText[] setUIText;
    private string dataPath;
    public Text[] contents;
    public string[] keys;
    public string fileLocation;
    private Lang Lman;

    // Use this for initialization

    void Awake() {

        dataPath = Application.streamingAssetsPath;
    }

    void Start() {

        myToggle = gameObject.GetComponent<Toggle>();

        setUIText = new SetUIText[9];

        for(int i = 0; i < contents.Length; i++) {
            setUIText[i] = contents[i].gameObject.GetComponent<SetUIText>();
           
        }
         
	}
	
	public void OnToggleChange() {

        if (myToggle.isOn) {
            
            ChangeKeys();
            SetNewText();
        }
    }


    private void ChangeKeys() {

        for (int i = 0; i < contents.Length; i++) {
            setUIText[i].key = keys[i];
           
        }
        
    }
    
    private void SetNewText() {

        Lman = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation, false);
        Lman.setLanguage(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation);
        
        foreach (SetUIText sendLanguageInfo in setUIText) {

            sendLanguageInfo.GetComponent<Text>().text = Lman.getString(sendLanguageInfo.key);

        }

    }
}
