﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetUserInputDropdown : MonoBehaviour {

    private Dropdown myDropdown;
    public string dropdownKey;
    public string dropdownKeyString;
    public Text label;

    void Start() {
         
        StartCoroutine(WaitToDisplayInfo());
         
    }

    IEnumerator WaitToDisplayInfo() {

        yield return new WaitForSeconds(.001f);

        myDropdown = gameObject.GetComponent<Dropdown>();

        if (PlayerPrefs.HasKey(dropdownKey) &&  PlayerPrefs.HasKey(dropdownKeyString)) {

            myDropdown.value = PlayerPrefs.GetInt(dropdownKey);
                       
            myDropdown.RefreshShownValue();

          
        }

    }


}
     
 
