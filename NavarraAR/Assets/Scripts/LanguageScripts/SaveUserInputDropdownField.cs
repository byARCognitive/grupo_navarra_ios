﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using UnityEngine.EventSystems;
using System;

public class SaveUserInputDropdownField : MonoBehaviour, IPointerUpHandler {

    private Dropdown dropDown;
    private List<string> keys = new List<string>();
    private Lang LmanItems;
    private string dataPath;
    private List<string> allStrings = new List<string>();
    private bool firstTouch;
    private bool firstTouchElement;
    private int flag = 0;
 

    public GameObject selectedText;

    public string key;
    //public GameObject firstElement; 

    public string fileLocation;

    public Text label;
    // Use this for initialization

    void Awake() {

        dataPath = Application.streamingAssetsPath;
        firstTouch = true;
        firstTouchElement = true;
    
    }

    void Start() {


        // Finds the Dropdown component
        dropDown = gameObject.GetComponent<Dropdown>();
        // Creates an empty list to populate the dropdown         
        allStrings = ChangeItemsLanguage();

    }



    public void OnPointerUp(PointerEventData eventData) {
        flag = 1;
        //      RemoveFirstElement();

        dropDown.ClearOptions();
        dropDown.AddOptions(allStrings);
        firstTouch = false;

    }

    private List<string> ChangeItemsLanguage() {

      
        LmanItems = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation, false);

        // Gets all strings associated with the chosen language
        allStrings = LmanItems.getAllStrings();
        keys = LmanItems.getAllKeys();

        if (flag == 1) {

            //    RemoveFirstElement();
            dropDown.options.RemoveAt(0);
            flag = 1;
        }

        // Add the list to the dropdown
        dropDown.ClearOptions();
        dropDown.AddOptions(allStrings);

        return allStrings;
    }

    private void RemoveFirstElement() {

        if (allStrings[0].Equals("SECTOR")) {
            allStrings.RemoveAt(0);
        } else if (allStrings[0].Equals("AREA")) {
            allStrings.RemoveAt(0);
        } else if (allStrings[0].Equals("PAÍS")) {
            allStrings.RemoveAt(0);
        } else if (allStrings[0].Equals("COUNTRY")) {
            allStrings.RemoveAt(0);
        }

    }

    public void OnValueChanged() {
         
        Dictionary<string, string> dictionaryOfItems = LmanItems.getDictionary();
        int positionInList = dropDown.value;
        int positionInDictionary = positionInList;


        PlayerPrefs.SetString(key, keys[positionInDictionary]);
        PlayerPrefs.SetInt("Area Position", positionInList);
        PlayerPrefs.SetString("AreaText", label.text);

    }
}


