﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SetLanguage : MonoBehaviour {

    public string language;
    public string sceneToLoad;

	// Use this for initialization
	void Start () {
	
	}
	
	public void SetNewLanguage() {

        PlayerPrefs.SetString("language", language);
        SceneManager.LoadScene(sceneToLoad);

    }
}
