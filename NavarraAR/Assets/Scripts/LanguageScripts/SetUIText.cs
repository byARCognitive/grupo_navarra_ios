﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using UnityEngine.UI;

public class SetUIText : MonoBehaviour {

    
    public string key;
    private Lang Lman;
    private string text;
    private Text textUI;
    private string dataPath;
    public string fileLocation;

    void Awake() {

        //if (Application.platform == RuntimePlatform.Android) {
        //    //   dataPath = "jar:file://" + Application.dataPath + "!/assets/";
        //    //
        //    dataPath = Application.dataPath;
        //} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
        //    dataPath = Application.streamingAssetsPath;
        //}

        dataPath = Application.streamingAssetsPath ;
      
    }


    void Start() {

        Lman = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"),fileLocation, false);
        textUI = gameObject.GetComponent<Text>();
        textUI.text = Lman.getString(key);
    
    }
     
}