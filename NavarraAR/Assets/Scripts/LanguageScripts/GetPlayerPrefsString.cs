﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetPlayerPrefsString : MonoBehaviour {

    public string savedInfo; 


	// Use this for initialization
	void Start () {

        gameObject.GetComponent<Text>().text = PlayerPrefs.GetString(savedInfo);

	}
	
	 
}
