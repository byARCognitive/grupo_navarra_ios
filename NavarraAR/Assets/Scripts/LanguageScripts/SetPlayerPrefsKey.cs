﻿using UnityEngine;
using System.Collections;

public class SetPlayerPrefsKey : MonoBehaviour {

    public string key;
    public string value;
     
    public void SavePrefs() {

        PlayerPrefs.SetString(key, value);

    }
}
