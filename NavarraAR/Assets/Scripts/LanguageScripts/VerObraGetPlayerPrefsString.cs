﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class VerObraGetPlayerPrefsString : MonoBehaviour {

    public string savedInfo;
    public GameObject parent;


    // Use this for initialization
    void Start () {


        EventManager.Instance.AddListener(EVENT_TYPE.CHANGE_PORTFOLIO_OBRAS_DESCRICAO, OnEvent);


        if (PlayerPrefs.GetString(savedInfo).Equals("")) {

            parent.SetActive(false);
        }else {

            gameObject.GetComponent<Text>().text = PlayerPrefs.GetString(savedInfo);
        }

       

    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
        switch (Event_Type) {
            case EVENT_TYPE.CHANGE_PORTFOLIO_OBRAS_DESCRICAO:
                PlayerPreferences();
                break;
        }
}

    private void PlayerPreferences() {
        if (PlayerPrefs.GetString(savedInfo).Equals("")) {

            parent.SetActive(false);
        } else {

            gameObject.GetComponent<Text>().text = PlayerPrefs.GetString(savedInfo);
        }

    }
}
