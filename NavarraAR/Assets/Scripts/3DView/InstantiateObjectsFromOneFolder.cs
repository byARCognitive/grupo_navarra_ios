﻿using UnityEngine;
using System.Collections;

public class InstantiateObjectsFromOneFolder : MonoBehaviour {

    private GameObject perfilObject;
    public Transform parent;

    public GameObject PerfilObject {
        get {
            return perfilObject;
        }
 
    }

	// Use this for initialization
	void Start () {

        

        Quaternion quatRotation = Quaternion.Euler(0, -45, 0);
        Vector3 vectPosition = new Vector3(-0.99f, 0, 0.78f);
        Vector3 localScale = new Vector3(0.006891027f, 0.006891027f, 0.006891027f);

		perfilObject = Resources.Load<GameObject>("RealidadeAumentada/" + PlayerPrefs.GetString("aluminioPerfil"));
 
        perfilObject.transform.position = vectPosition;
        perfilObject.transform.rotation = quatRotation;
        perfilObject.transform.localScale = localScale;
      

        GameObject perfil = Instantiate(perfilObject);

       
         
    }
 
}
