﻿using UnityEngine;
using System.Collections;

public class FadeOutAnimationModel : MonoBehaviour {

    private bool trigger;
    private Color whiteColor;
    public Material[] mats;

	// Use this for initialization
	void Start () {
    

	}
	
	// Update is called once per frame
	void Update () {

        if (trigger) {

            for(int i = 0; i < mats.Length; i++) {

                mats[i].color = Color.Lerp(mats[i].color, Color.white, 1);

            }

        }

	}
      

    private void Fade(float valueFrom, float valueTo, float timeLeft, Renderer[] render) {

        foreach (Renderer rend in render) {

            foreach (Material mat in rend.materials) {

                mat.color = new Color(Mathf.Lerp(valueFrom, valueTo, timeLeft), mat.color.g, mat.color.b, Mathf.Lerp(valueFrom, valueTo, timeLeft));
                 
                 
            }
     
        }

    }
}
