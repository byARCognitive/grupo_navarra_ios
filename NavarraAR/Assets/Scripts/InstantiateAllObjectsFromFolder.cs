﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InstantiateAllObjectsFromFolder : MonoBehaviour {

    public string folderName;
    private ToggleGroup toggleGroup;
    private int aux;
  //  private GameObject[] imagesName;


    // Use this for initialization
    void Start() {

        toggleGroup = gameObject.GetComponent<ToggleGroup>();

        StartCoroutine(Wait());

    }


    private IEnumerator Wait() {

        yield return new WaitForSeconds(.5f);
 
        if (PlayerPrefs.HasKey("loadPortfolioDeObras")) {

             GameObject[] toggleImages = Resources.LoadAll<GameObject>("PortfolioDeObrasAUX/Obras/" + PlayerPrefs.GetString("loadPortfolioDeObras"));

            

            for (int i = 0; i < toggleImages.Length; i++) {

               


                if (toggleImages[i].name != "GNRATION_2" && toggleImages[i].name != "IPCA_2" && toggleImages[i].name != "BIOCANT_2" ) 
                    {
                    GameObject image = Instantiate(toggleImages[i]);
                    image.transform.SetParent(gameObject.transform, false);
                } else {

                    if (PlayerPrefs.GetString("loadPortfolioDeObras") != "") {
                        GameObject image = Instantiate(toggleImages[i]);
                        image.transform.SetParent(gameObject.transform, false);
                    }

                }
            }

        }
        else {

            GameObject[] toggleImages = Resources.LoadAll<GameObject>("PortfolioDeObrasAUX/Obras/");
           

            for (int i = 0; i < toggleImages.Length; i++) {
                GameObject image = Instantiate(toggleImages[i]);
                
                image.transform.SetParent(gameObject.transform, false);


            }

        }

        Toggle tog = gameObject.GetComponentInChildren<Toggle>();
        tog.isOn = true;
         

    }


}
