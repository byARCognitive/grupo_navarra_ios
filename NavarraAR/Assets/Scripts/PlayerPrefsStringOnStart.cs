﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsStringOnStart : MonoBehaviour {
    public string key;
    public string value;
    // Use this for initialization
    void Start () {
        PlayerPrefs.SetString(key, value);
    }
	
	 
}
