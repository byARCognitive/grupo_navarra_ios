﻿using UnityEngine;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;
using System.IO;

public class SendEmailRequestSpecialMarker : MonoBehaviour {

    //private string originEmail = "web@navarraaluminio.com";
    private string originEmail = "web";
    private string password = "NavarraWEB";
    private string messageBody = "";
    private string dataPath;
    public string[] playerPrefs;
	private Button myButton;
     
    public string fileLocation;
 
    private Lang Lman;
    private string subject;


    public GameObject emailSentWithSuccess;
    public GameObject emailFailed;
    public GameObject parent;

    public static bool emailActive;
    public static bool ciclo;
    public GameObject emailIsWrong;


	public Sprite pedirAcessoActivePT;
	public Sprite pedirAcessoActiveUK;
	public Sprite pedirAcessoNormalPT;
	public Sprite pedirAcessoNormalUK;


	private SpriteState sprState;

    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
            + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


    public static bool IsEmail(string email) {
        if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
        else return false;
    }


    private static void SendCompleteCallback(object sender, AsyncCompletedEventArgs e) {
        string token = (string)e.UserState;
        if (e.Cancelled) {
            emailActive = false;
            Debug.Log(token + " Send canceled.");
        }
        if (e.Error != null) {
            emailActive = false;
            Debug.Log("[" + token + "] " + e.Error.ToString());
        } else {

            Debug.Log("ENVIADO!");
            emailActive = true;
        }
        ciclo = false;

    }

    void Awake() {

        //if (Application.platform == RuntimePlatform.Android) {
        //    //   dataPath = "jar:file://" + Application.dataPath + "!/assets/";
        //    //
        //    dataPath = Application.dataPath;
        //} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
        //    dataPath = Application.streamingAssetsPath;
        //}

        dataPath = Application.streamingAssetsPath;

    }

    void Start() {
		ciclo = true;
        Lman = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation, false);
		myButton = gameObject.GetComponent<Button>();



		if (PlayerPrefs.GetString("language") == "Portuguese") {

			sprState.pressedSprite = pedirAcessoActivePT;
			myButton.spriteState = sprState;

		} else {

			sprState.pressedSprite = pedirAcessoActiveUK;
			myButton.spriteState = sprState;
		}





    }

    public void SendEmailNow(string body) {

        if (PlayerPrefs.GetString("Email").Length > 3) {

		 
            subject = Lman.getString("pedido_marcador_especial");

            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;


            //COM SPAM, É PRECISO MUDAR O LINK DA IMAGEM
			messageBody = "<meta charset=\"utf-8\"/><center><table style=\"width:700px;font-family:Arial,sans-serif\"><tr><td><a href=\"http://www.navarraaluminio.com/pt/grupo-industrial/perfil-1-parceiros-cliente/informacoes-perfis/documentos.html\" target=\"_blank\"><img border=\"0\" height =\"244\" src =\"https://s29.postimg.org/onf3kblmv/image005.jpg\" width=\"700\"></a></td></tr><tr><td style =\"height:20px\"></td></tr><tr><td style =\"background-color:#CCCCCC;text-align:left;padding:20px;margin:30px;font-family:font-family:Arial,sans-serif;font-size:12pt\">";

            // SEM SPAM
            // messageBody = "<meta charset=\"utf-8\"/><center><table style=\"width:700px;font-family:Arial,sans-serif\"> width=\"700\"></a></td></tr><tr><td style =\"height:20px\"></td></tr><tr><td style =\"background-color:#CCCCCC;text-align:left;padding:20px;margin:30px;font-family:font-family:Arial,sans-serif;font-size:12pt\">";

             if(PlayerPrefs.GetString("language") == "Portuguese") {
				
				messageBody = messageBody + "<color=black></color>" +  "<b>" +
                "NOME:" + "</b>" + "  " + PlayerPrefs.GetString(playerPrefs[0]) + "<br/><b>" + "EMAIL:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[1]) + "<br/><b>" +
                "SETOR:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[2]) + "<br/><b>" + "EMPRESA:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[3]) + "<br/><b>" +
                "PROFISSÃO:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[4]) + "<br/><b>" + "PAÍS:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[5]) + "<br/><b>" +
                "LOCALIDADE:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[6]) + "<br/>" + "<br/> <b>Assunto/Subject: </b>" + "Acesso aos ficheiros BIM/DXF.";


            }else {

				messageBody =  messageBody + "<color=black></color>" + "<b>" + "NAME:" + "</b>" + PlayerPrefs.GetString(playerPrefs[0]) + "<br/><b>" + "EMAIL:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[1]) + "<br/><b>" +
                "MARKET:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[2]) + "<br/><b>" + "COMPANY" + "</b>  " + PlayerPrefs.GetString(playerPrefs[3]) + "<br/><b>" +
                "FUNCTION:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[4]) + "<br/><b>" + "COUNTRY:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[5]) + "<br/><b>" +
                "DISTRICT/REGION:" + "</b>  " + PlayerPrefs.GetString(playerPrefs[6]) + "<br/>" + "<br/> <b>Assunto/Subject: </b>" + "Access to BIM/DXF files.";
       
             }

         
            messageBody = messageBody + "<br/><br/></tr><tr><td style =\"text-align:right;background-color:#E1E1E1;padding:10px;font-size:8.5pt;color:#9A9A9A\">© 2016 Navarra | todos os direitos reservados | all rights reserved</td></tr></table></center>";
            mail.From = new MailAddress("web@navarraaluminio.com");

          //   mail.To.Add("web@navarraaluminio.com");
              mail.To.Add(PlayerPrefs.GetString(playerPrefs[1]));
            mail.CC.Add(PlayerPrefs.GetString(playerPrefs[1]));


            mail.Subject = subject;
            mail.Body = messageBody;

            SmtpClient smtpServer = new SmtpClient();
            smtpServer.Host = "62.28.224.83";
            smtpServer.Port = 2525;
            smtpServer.Credentials = new System.Net.NetworkCredential(originEmail, password) as ICredentialsByHost;
            smtpServer.EnableSsl = true;
            smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain Chain, SslPolicyErrors sslPolicyErrors) { return true; };

            ciclo = true;

            smtpServer.SendCompleted += new SendCompletedEventHandler(SendCompleteCallback);

            Debug.Log("Sending Email");
            smtpServer.SendAsync(mail, "byARToken");


	//		StartCoroutine (sendResponseToUser());




		} else {

			emailIsWrong.SetActive(true);

		}

          

    }


	void Update(){


		if(ciclo == false){
 
			if (PlayerPrefs.GetString("language") == "Portuguese") {

				myButton.gameObject.GetComponent<Image>().sprite = pedirAcessoNormalPT;
				myButton.spriteState = sprState;

			} else {

				myButton.gameObject.GetComponent<Image>().sprite = pedirAcessoNormalUK;
				myButton.spriteState = sprState;
			}
 


			if (emailActive) {
				Debug.Log("Email Activo");
				emailSentWithSuccess.SetActive(true);
				parent.SetActive(false);
				// emailActive = false;
				
			} else {
				Debug.Log("Email Desactivado");
				emailFailed.SetActive(true);
				parent.SetActive(false);
			}

			ciclo = true;

		}


		 

	}

	IEnumerator sendResponseToUser(){


		yield return new WaitForSeconds (7);

 

		 

		if (emailActive) {
			Debug.Log("Email Activo");
			emailSentWithSuccess.SetActive(true);

			// emailActive = false;

		} else {
			Debug.Log("Email Desactivado");
			emailFailed.SetActive(true);
		}

		parent.SetActive(false);
		//   emailSuccess.SetActive(true);



	}	



}
