﻿using UnityEngine;
using System.Collections;
using System;

public class DeactivateGO : MonoBehaviour {

    public GameObject objectToDeactivate;

    public void OnClickDeactivate() {

        objectToDeactivate.SetActive(false);

    }

    

}
