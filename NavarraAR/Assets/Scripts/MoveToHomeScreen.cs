﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MoveToHomeScreen : MonoBehaviour {

    public string industrialScene;
    public string arquitecturaScene;

    public void ChangeScreenScene() {

        if (PlayerPrefs.GetString("chooseSector").Equals("Industrial")) {

            SceneManager.LoadScene(industrialScene);

        } else {

            SceneManager.LoadScene(arquitecturaScene);

        }
    }

}