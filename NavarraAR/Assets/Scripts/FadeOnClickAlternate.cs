﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeOnClickAlternate : MonoBehaviour {

    public Image myImage;
    public float initialValue;
    public float toValue;
    public float duration;
     
    // Use this for initialization
    public void Fade() {

        if (!myImage.gameObject.activeSelf) {
            myImage.gameObject.SetActive(true);

        }
        myImage.canvasRenderer.SetAlpha(initialValue);
        myImage.CrossFadeAlpha(toValue, duration, false);

        StartCoroutine(ChangeScreenToNew());
    }

    private IEnumerator ChangeScreenToNew() {
        yield return new WaitForSeconds(duration);

        if(PlayerPrefs.GetString("lastScreen") == "realidadeAumentada") {

            SceneManager.LoadScene("RealidadeAumentadaArquitecturaV2");

        } else {

            SceneManager.LoadScene("SistemasNavarraV2");

        }

        
    }
}
