﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeOutBackground : MonoBehaviour {

	public Image image;

	// Use this for initialization
	void Start() {

		image.canvasRenderer.SetAlpha(1.0f);

		StartCoroutine(Waiting());


	}

	private IEnumerator Waiting() {
		yield return new WaitForSeconds(2);
		image.CrossFadeAlpha(0.0f, .5f, false);
	}
}
