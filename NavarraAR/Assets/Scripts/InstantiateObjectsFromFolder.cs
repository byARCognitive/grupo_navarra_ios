﻿using UnityEngine;
using System.Collections;

public class InstantiateObjectsFromFolder : MonoBehaviour {

    public string folderName;
    public GameObject parent;

    // Use this for initialization
    void Start() {

        
        StartCoroutine(Wait());
         
    }


    private IEnumerator Wait() {

        yield return new WaitForSeconds(.8f);

        // Get all Objects in folder
        GameObject[] objectsInFolder = Resources.LoadAll<GameObject>(folderName);

        for (int i = 0; i < objectsInFolder.Length; i++) {
            GameObject image = Instantiate(objectsInFolder[i]);
            image.transform.SetParent(parent.transform, false);


        }
    }
}
