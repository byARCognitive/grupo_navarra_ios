﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScreen : MonoBehaviour {

    public string nextScene;
 	
	public void Change() {
       SceneManager.LoadScene(nextScene);
    }
}
