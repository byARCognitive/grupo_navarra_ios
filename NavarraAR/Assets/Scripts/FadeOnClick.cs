﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class FadeOnClick : MonoBehaviour {

    public Image myImage;
    public float initialValue;
    public float toValue;
    public float duration;
    public string sceneName;
    // Use this for initialization
    public void Fade() {

        if (!myImage.gameObject.activeSelf) {
            myImage.gameObject.SetActive(true);

        }
        myImage.canvasRenderer.SetAlpha(initialValue);
        myImage.CrossFadeAlpha(toValue, duration, false);

        StartCoroutine(ChangeScreenToNew());
    }

    private IEnumerator ChangeScreenToNew() {
        yield return new WaitForSeconds(duration);

        SceneManager.LoadScene(sceneName);
    }
}
