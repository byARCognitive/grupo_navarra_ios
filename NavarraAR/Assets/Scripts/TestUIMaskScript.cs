﻿using UnityEngine;
using System.Collections;

public class TestUIMaskScript : MonoBehaviour {

    public GameObject toHide;

	// Use this for initialization
	void Start () {
        toHide.transform.SetParent(gameObject.transform, false);

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
