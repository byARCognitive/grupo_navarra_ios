﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class RealidadeAumentadaVerObra : MonoBehaviour {

    private Image myImage;
    private string perfilName;
    private string fileLocation;
    public string nextScene;
    public string verObraScene;
    public GameObject noProjectsAssociated;
    private ChangeLanguageKeyOfDescriptionPortfolioDeObras changeLangKeyOf;
    private Lang Lman;
    private string dataPath;
    private string[] keys = { "titulo", "gabinete", "localizacao", "sistemas", "tipologia", "serralharia", "dono_da_obra" };
    public Sprite mySprite;
    // Use this for initialization
    void Start () {
        myImage = gameObject.GetComponent<Image>();
        EventManager.Instance.AddListener(EVENT_TYPE.UPDATE_BUTTONS_INFO, OnEvent);

	}

    void Awake() {
        dataPath = Application.streamingAssetsPath;
    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
        switch (Event_Type) {
            case EVENT_TYPE.UPDATE_BUTTONS_INFO:
                perfilName = Sender.name;
                break;
        }
    }

    public void OnClick() {
         
        // Check if file exists
        Debug.Log("PortfolioDeObrasImagens/Images/" + perfilName);

        GameObject[] toggleImages = Resources.LoadAll<GameObject>("PortfolioDeObrasAUX/Obras/" + perfilName);


        if (toggleImages.Length == 1) {


            changeLangKeyOf = toggleImages[0].GetComponent<ChangeLanguageKeyOfDescriptionPortfolioDeObras>();

            fileLocation = changeLangKeyOf.fileLocation;

            Lman = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation, false);

            PlayerPrefs.SetString("gabineteObject", Lman.getString(keys[1]));
            PlayerPrefs.SetString("localizacaoObject", Lman.getString(keys[2]));
            PlayerPrefs.SetString("sistemasObject", Lman.getString(keys[3]));
            PlayerPrefs.SetString("tipologiaObject", Lman.getString(keys[4]));
            PlayerPrefs.SetString("serralhariaObject", Lman.getString(keys[5]));
            PlayerPrefs.SetString("dono_da_obraObject", Lman.getString(keys[6]));
            PlayerPrefs.SetString("obraTitleName", Lman.getString(keys[0]));

            PlayerPrefs.SetString("obra", "");

            PlayerPrefs.SetString("perfilAluminio", perfilName);

            PlayerPrefs.SetString("loadPortfolioDeObras", perfilName);

            SceneManager.LoadScene(verObraScene);


        } else
        if (toggleImages.Length > 1) {


            PlayerPrefs.SetString("loadPortfolioDeObras", perfilName);
            //   PlayerPrefs.SetString("obra", "");

            SceneManager.LoadScene(nextScene);

             

        } else{

            //File does not contain images
            myImage.sprite = mySprite;
            noProjectsAssociated.SetActive(true);


        }
         
    }
}
