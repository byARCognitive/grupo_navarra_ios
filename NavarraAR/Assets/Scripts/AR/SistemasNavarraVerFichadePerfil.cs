﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SistemasNavarraVerFichadePerfil : MonoBehaviour {

    public GameObject parent;
    private SistemasNavarraContentPDFBIMDXF sist;
    public Image myImage;
  

    private bool hasProfileLanguage;
    private string parentNameProfile;
 
 

    public string ParentNameProfile {
        get {
            return parentNameProfile;
        }
        set {
            parentNameProfile = value;
        }
    }

     
    // Use this for initialization
    void Start () {

        parentNameProfile = parent.name + "Profile";
        parentNameProfile = parentNameProfile.Replace("(Clone)", "");

        sist = parent.GetComponent<SistemasNavarraContentPDFBIMDXF>();

        if (PlayerPrefs.GetString("language").Equals("Portuguese")) {

            hasProfileLanguage = sist.hasProfileInfoAssociated_PT;
            parentNameProfile = parentNameProfile + "PT";

        } else {

            hasProfileLanguage = sist.hasProfileInfoAssociated_UK;
            parentNameProfile = parentNameProfile + "UK";
        }

        if (hasProfileLanguage) {

            // color white

            myImage.color = new Color(255, 255, 255, 1);

        } else {

            // color grey
            myImage.color = new Color(255, 255, 255, .3019f);
        }
 

    }
	
	public void OnClick() {


        if (hasProfileLanguage) {


            //Instanciar Painel com ecrã associado ao perfil
            
            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_PROFILE_IMAGE, this);


        } else {

            // Enviar evento a dizer que não tem folha de perfil associada
            
            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_NO_PROFILE_IMAGE, this);

        }
     
} 

}
