﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class SistemasNavarraPDF : MonoBehaviour {

    public GameObject parent;
    public Sprite selectedButton;
    public Sprite notSelectedButton;

    private SistemasNavarraContentPDFBIMDXF sist;
    public Image myImage;
     
    private bool hasPDFLanguage;

    private bool itemSelected;

    private string parentNamePDF;

    public string ParentNamePDF {
        get{
            return parentNamePDF;
        }set {
            parentNamePDF = value;
        }
    }

 
    // Use this for initialization
    void Start () {

        parentNamePDF = parent.name + "PDF";
        parentNamePDF = parentNamePDF.Replace("(Clone)", "");
         
        itemSelected = false;

        sist = parent.GetComponent<SistemasNavarraContentPDFBIMDXF>();
        

        if (PlayerPrefs.GetString("language").Equals("Portuguese")) {
            hasPDFLanguage = sist.hasPDF_PT;
        } else {
            hasPDFLanguage = sist.hasPDF_UK;
        }
        



        if (hasPDFLanguage) {

            // color white
            
            myImage.color = new Color(255,255,255, 1);

        }else {

            // color grey
            myImage.color = new Color(255, 255, 255, .3019f);
        }


	}
	
	public void OnclickPDF() {
     
        if (hasPDFLanguage) {

            // if the item is not selected, send an Event to the Manager, to add it to the List.
            // If the item is selected, send an Event to the Manager, to remove it from the List.

            if (!itemSelected) {

                //item selected Image
                myImage.sprite = selectedButton;

                EventManager.Instance.PostNotification(EVENT_TYPE.ADD_ITEM_TO_SISTEMAS_NAVARRA, this, parentNamePDF);

            }else {

                //item not selected Image
                myImage.sprite = notSelectedButton;

                EventManager.Instance.PostNotification(EVENT_TYPE.REMOVE_ITEM_FROM_SISTEMAS_NAVARRA, this, parentNamePDF);

            }

            itemSelected = !itemSelected;
 

        } else {


            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_MARKER_INFO_NO_FILE, this);  


        }
    }
}
