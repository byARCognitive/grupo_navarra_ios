﻿/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using System;
using System.Collections;
using UnityEngine;

namespace Vuforia {
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class NavarraArquitecturaDefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler {
        #region PRIVATE_MEMBER_VARIABLES

        private TrackableBehaviour mTrackableBehaviour;

        private bool hasPdf = false;
        private bool hasBim = false;
        private bool hasdxf = false;
        private bool isFavorite = false;

        public static bool flag = false;

        private string pdfName;
        private string bimName;
        private string dxfName;

        private bool flagTest;
        private Lang lang;

        private SistemasNavarraContentPDFBIMDXF sistNavarraContent;

        #endregion // PRIVATE_MEMBER_VARIABLES

        private GameObject leftPanelARQ;
        private GameObject leftPanelIND;
        public GameObject child;
        private GameObject Loading;

		private bool isInstantiated;


        private GameObject resourcesObject;
        private bool hasPDFLanguage;

        public string PDFName {
            get {
                return pdfName;
            }
            set {
                pdfName = value;
            }
        }

        public string BIMName {
            get {
                return bimName;
            }
            set {
                bimName = value;
            }
        }

        public string DXFName {
            get {
                return dxfName;
            }
            set {
                dxfName = value;
            }
        }

        public bool HasPdf {
            get {
                return hasPdf;
            }
            set {
                hasPdf = value;
            }
        }

        public bool HasBim {
            get {
                return hasBim;
            }
            set {
                hasBim = value;
            }
        }

        public bool Hasdxf {
            get {
                return hasdxf;
            }
            set {
                hasdxf = value;
            }
        }

        public bool IsFavorite {
            get {
                return isFavorite;
            }
            set {
                isFavorite = value;
            }
        }


        #region UNTIY_MONOBEHAVIOUR_METHODS

        void Awake() {


		    leftPanelARQ = GameObject.FindGameObjectWithTag ("leftPanelARQ");
			leftPanelIND = GameObject.FindGameObjectWithTag ("leftPanelIND");
			Loading = GameObject.FindGameObjectWithTag ("Loading");
		    
            
			// seems if I do this then OnTrackableStateChanged will only run OnTrackingLost during this scene and not as a result 
            // of this scene unloading to another one
            // stops the unable to set hint, deinitialise tracker etc and allows AR to continue restarting between scenes
            PlayerPrefs.SetInt("LoadingLevel", 0);

        }

        void Start() {
                       
			isInstantiated = false;

            PDFName = gameObject.name + "PDF";
            BIMName = gameObject.name + "BIM";
            DXFName = gameObject.name + "DXF";

            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour) {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }

 
                // Get's info from Resources Specific Item
            resourcesObject = Resources.Load<GameObject>("ProdutosSistemaNavarra/" + gameObject.name);

            sistNavarraContent = resourcesObject.GetComponentInChildren<SistemasNavarraContentPDFBIMDXF>();


            if (PlayerPrefs.GetString("language").Equals("Portuguese")) {
                hasPDFLanguage = sistNavarraContent.hasPDF_PT;
            } else {
                hasPDFLanguage = sistNavarraContent.hasPDF_UK;
            }



            HasPdf = hasPDFLanguage;
            HasBim = sistNavarraContent.hasBIM;
            Hasdxf = sistNavarraContent.hasDXF;
            IsFavorite = sistNavarraContent.isFavorite;
             

        }
         

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS


        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus) {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
                OnTrackingFound();
            } else {
                if (PlayerPrefs.GetInt("LoadingLevel") == 0) {
                    OnTrackingLost();
                }
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound() {

		 

			 
			Loading.SetActive(true);
			 


            PlayerPrefs.SetString("aluminioPerfil", gameObject.name);

            // Aparece imagem de loading
           

                resourcesObject = Resources.Load<GameObject>("ProdutosSistemaNavarra/" + gameObject.name);
         
                sistNavarraContent = resourcesObject.GetComponentInChildren<SistemasNavarraContentPDFBIMDXF>();

                HasPdf = hasPDFLanguage;
                HasBim = sistNavarraContent.hasBIM;
                Hasdxf = sistNavarraContent.hasDXF;
                IsFavorite = sistNavarraContent.isFavorite;

     
            // Activate Child object

            // if (!flag) {
            //   Debug.Log("OUT");

                flag = true;

            //} else {

                

                leftPanelARQ.SetActive(true);
                leftPanelIND.SetActive(false); 

                child.SetActive(true);

                flagTest = false;

                StartCoroutine(Load3DModel());    

             
                
            //   }

            

        }

        private IEnumerator Load3DModel() {

            yield return new WaitForSeconds(1);


			if(!isInstantiated){

				GameObject childModel = Instantiate (Resources.Load ("RealidadeAumentada/" + gameObject.name)) as GameObject;
				string childModelName = childModel.name.Replace ("(Clone)", "");
				childModel.name = childModelName;
				childModel.transform.SetParent (child.transform, false);
				childModel.tag = "perfisModel";

				isInstantiated = true;
			}
 


            Loading.SetActive(false);
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);


            // Enable rendering:
            foreach (Renderer component in rendererComponents) {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents) {
                component.enabled = true;
            }

            //Desligar Imagem de Loading
             

            StartCoroutine(WaitToInitialize());

          //  Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
        }

        private IEnumerator WaitToInitialize() {

            yield return new WaitForSeconds(.01f);
            
            EventManager.Instance.PostNotification(EVENT_TYPE.UPDATE_BUTTONS_INFO, this);
            
        }

        private void OnTrackingLost() {

            Time.timeScale = 1;
            StopTime.timeIsStopped = false;

            
		    GameObject[] models = GameObject.FindGameObjectsWithTag ("perfisModel");

			for(int i = 0; i < models.Length; i++){
			Destroy (models [i]);
			}

			isInstantiated = false;

			child.SetActive(false);



            if(leftPanelARQ != null) {
            leftPanelARQ.SetActive(false);

            }

		//	if(Loading != null)
            Loading.SetActive(false);

            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents) {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents) {
                component.enabled = false;
            }


            // Deactivate Child object


     //       Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
        }



        #endregion // PRIVATE_METHODS
    }
}
