﻿using UnityEngine;
using System.Collections;

public class HideTutorial : MonoBehaviour {

    public GameObject tutorialPanel;

	// Use this for initialization
	void Start () {
	    
        if(PlayerPrefs.GetInt("hideTutorial") == 1) {
            tutorialPanel.SetActive(false);
            PlayerPrefs.SetInt("hideTutorial",0);
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
