﻿/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using System;
using System.Collections;
using UnityEngine;

namespace Vuforia {
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class NavarraFileUnblocker : MonoBehaviour,
                                                ITrackableEventHandler {
        #region PRIVATE_MEMBER_VARIABLES

        private TrackableBehaviour mTrackableBehaviour;

        public static bool unlockFiles = false;
        public GameObject specialMarkerCheck;
        public GameObject Loading;

        public static bool flagTest;
        #endregion // PRIVATE_MEMBER_VARIABLES

        public GameObject leftPanel;

        #region UNTIY_MONOBEHAVIOUR_METHODS

        void Start() {

            flagTest = true;

            if (PlayerPrefs.HasKey("unlockFiles")) {
                unlockFiles = true;
            }

 
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour) {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
             
        }

         
        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus) {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
                OnTrackingFound();
            } else {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound() {

            // Activate Child object

            if (flagTest) {

                flagTest = false;

            }else{

                Loading.SetActive(true);
                //Info a indicar quando se detecta este marker

                //Esconder o painel do lado
                leftPanel.SetActive(false);


                unlockFiles = true;
                PlayerPrefs.SetInt("unlockFiles", 1);



                Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
                Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);


                //Start Loading Animation

                //Send Event to all buttons so they can change their appearance according to the info sent
                //    EventManager.Instance.PostNotification(EVENT_TYPE.UPDATE_BUTTONS_INFO, this);

                // Enable rendering:
                foreach (Renderer component in rendererComponents) {
                    component.enabled = true;
                }

                // Enable colliders:
                foreach (Collider component in colliderComponents) {
                    component.enabled = true;
                }


                StartCoroutine(LoadSpecialMarker());
                //End Loading Animation

                Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

          //      EventManager.Instance.PostNotification(EVENT_TYPE.UPDATE_BUTTONS_INFO, this);

            }

             

        }

        private IEnumerator LoadSpecialMarker() {

            yield return new WaitForSeconds(1);
            Loading.SetActive(false);
            specialMarkerCheck.SetActive(true);
        }

        private void OnTrackingLost() {

            flagTest = false;
            leftPanel.SetActive(true);

            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents) {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents) {
                component.enabled = false;
            }


            // Deactivate Child object


            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
        }
        

        #endregion // PRIVATE_METHODS
    }
}
