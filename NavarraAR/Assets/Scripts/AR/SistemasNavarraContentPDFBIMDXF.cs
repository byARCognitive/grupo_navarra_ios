﻿using UnityEngine;
using System.Collections;

public class SistemasNavarraContentPDFBIMDXF : MonoBehaviour {

    public bool hasPDF_PT;
    public bool hasPDF_UK;
    public bool hasBIM;
    public bool hasDXF;
    public bool isFavorite;
    public bool hasProjectAssociated;
    public bool hasProfileInfoAssociated_PT;
    public bool hasProfileInfoAssociated_UK;
 
}
