﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class FavoriteInfoRealidadeAumentada : MonoBehaviour {

    private NavarraArquitecturaDefaultTrackableEventHandler arquitectureDefaultTracker;
    private UnityEngine.UI.Image myImage;
    private string fileName;
    private GameObject objectInResources;
    private SistemasNavarraContentPDFBIMDXF sistNavarraContent;
    public Sprite mySelectedSprite;
    public Sprite myNotSelectedSprite;
    private int preferences;

    // Use this for initialization
    void Start() {

        EventManager.Instance.AddListener(EVENT_TYPE.UPDATE_BUTTONS_INFO, OnEvent);

        myImage = gameObject.GetComponent<UnityEngine.UI.Image>();
    }


    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {

        switch (Event_Type) {

            case EVENT_TYPE.UPDATE_BUTTONS_INFO:

                arquitectureDefaultTracker = Sender.GetComponent<NavarraArquitecturaDefaultTrackableEventHandler>();
                fileName = arquitectureDefaultTracker.name;
                preferences = PlayerPrefs.GetInt(fileName + "dataSistNav",0);
                //Check PDF Property

                if (preferences == 1) {

                    //If true, color white
                    myImage.sprite = mySelectedSprite;

                } else {

                    //    //If false, color grey
                    myImage.sprite = myNotSelectedSprite;
                }

                //if (arquitectureDefaultTracker.IsFavorite) {

                //    //If true, color white
                //    myImage.sprite = mySelectedSprite;

                //} else {

                //    //    //If false, color grey
                //    myImage.sprite = myNotSelectedSprite;
                //}

                break;

        }


    }

    public void Onclick() {

        objectInResources = Resources.Load<GameObject>("ProdutosSistemaNavarra/" + fileName);

        Debug.Log("NAME  " + objectInResources.name);

        sistNavarraContent = objectInResources.GetComponentInChildren<SistemasNavarraContentPDFBIMDXF>();

        preferences = PlayerPrefs.GetInt(fileName + "dataSistNav", 0);

        if (preferences == 1) {

            Debug.Log("NotFavorite");
            //If favorite , then not favorite
            myImage.sprite = myNotSelectedSprite;

            // Vai ao file com o nome certo e no boleano dos favoritos, mete que n é favorito
            sistNavarraContent.isFavorite = false;
            PlayerPrefs.SetInt(fileName + "dataSistNav", 0);

        } else {
            Debug.Log("IsFavorite");
            // If not favorite, then favorite
            myImage.sprite = mySelectedSprite;

            // Vai ao file com o nome certo e no boleano dos favoritos, mete que é favorito
            sistNavarraContent.isFavorite = true;
            PlayerPrefs.SetInt(fileName + "dataSistNav", 1);
        }


    }
}