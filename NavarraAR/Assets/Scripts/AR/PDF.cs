﻿using UnityEngine;
using System.Collections;
using System;
using Vuforia;
using System.IO;
using UnityEngine.UI;

public class PDF : MonoBehaviour {

    private NavarraArquitecturaDefaultTrackableEventHandler arquitectureDefaultTracker;
    private string dataPath;
    private string pdfFileName;
    private UnityEngine.UI.Image myImage;

    public string PDFFileName {
        get{
            return pdfFileName;
        }set {
            pdfFileName = value;
        }
    }

    public GameObject fileDoesNotExist;
    public Sprite myShiningSprite;
  

    // Use this for initialization

    void Awake() {
        dataPath = Application.streamingAssetsPath;
    }

    // Use this for initialization
    void Start() {

        myImage = gameObject.GetComponent<UnityEngine.UI.Image>();

        EventManager.Instance.AddListener(EVENT_TYPE.UPDATE_BUTTONS_INFO, OnEvent);

    }
     

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {

        switch (Event_Type) {

            case EVENT_TYPE.UPDATE_BUTTONS_INFO:

                arquitectureDefaultTracker = Sender.GetComponent<NavarraArquitecturaDefaultTrackableEventHandler>();
                PDFFileName = arquitectureDefaultTracker.PDFName;

                  
                //Check PDF Property

                if (arquitectureDefaultTracker.HasPdf) {

                    //If true, color white
                    myImage.color = new Color(255, 255, 255, 1);

                } else {

                    //If false, color grey and not interactable
                     
                    myImage.color = new Color(255, 255, 255, .3019f);

                }


                break;

        }


    }


    public void OnPDFClick() {

            if (arquitectureDefaultTracker.HasPdf) {

         

            //If true, download file and change image to bright
            myImage.sprite = myShiningSprite;

            StartCoroutine(WaitTime());

           
             

           } else {

            
            //If false, a pop up appears saying that there is no pdf
            fileDoesNotExist.SetActive(true);

          }
    }

    private IEnumerator WaitTime() {

        yield return new WaitForSeconds(.01f);
     
        EventManager.Instance.PostNotification(EVENT_TYPE.SEND_PDF_TO_EMAIL, this);

    }
}
