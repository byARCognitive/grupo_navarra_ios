﻿using UnityEngine;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;
using System.IO;
using System;

public class SendAllLinksToEmail : MonoBehaviour {
	
	public GameObject emailSuccess;
	public GameObject emailFailed;
	
	public GameObject needToAddItem;
	
	private string password = "NavarraWEB";
	private string originEmail = "web";
	
	private string messageBody = "";
	private string dataPath;
	
	public static UnityEngine.Component comp;
	
	public string fileLocation;
	
	private Lang Lman;
	private string subject;
	
	public GameObject button;
	public Image myImage;
	public Sprite buttonNotDownloadingPT;
	public Sprite buttonNotDownloadingUK;
	public Sprite buttonDownloadingPT;
	public Sprite buttonDownloadingUK;
	private GameObject dumy1;
	private GameObject dumy2;
	
	public static bool emailActive;
	public static bool ciclo;
	
	public GameObject emailIsWrong;
	
	public const string MatchEmailPattern =
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
			+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
			+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
			+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
	
	
	public static bool IsEmail(string email) {
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}
	
	
	private static void SendCompleteCallback(object sender, AsyncCompletedEventArgs e) {
		string token = (string)e.UserState;
		
		
		if (e.Cancelled) {
			Debug.Log(token + " Send canceled.");
			
			emailActive = false;
			
		}
		if (e.Error != null) {
			Debug.Log("[" + token + "] " + e.Error.ToString());
			
			emailActive = false;
			
		} else {
			Debug.Log("ENVIADO!");
			
			emailActive = true;
			
		}
		ciclo = false;
		
	}
	
	
	void Awake() {
		
		
		
		//if (Application.platform == RuntimePlatform.Android) {
		//    //   dataPath = "jar:file://" + Application.dataPath + "!/assets/";
		//    //
		//    dataPath = Application.dataPath;
		//} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
		//    dataPath = Application.streamingAssetsPath;
		//}
		
		dataPath = Application.streamingAssetsPath;
		
	}
	
	void Start() {
		emailActive = false;
		ciclo = true;

		StartCoroutine (ChangeXML ());

		 
		
		
	}


	private IEnumerator ChangeXML(){

		WWW wwwXML = new WWW ("https://www.dropbox.com/s/ztflr9zsddt3xbz/ar_arquitectura_files.xml?dl=1");

		yield return wwwXML;

		Lman = new Lang(wwwXML.text, PlayerPrefs.GetString("language"), fileLocation, true);


	}


	public void SendEmailNow(string body) {
		
		if (PlayerPrefs.GetString("Email").Length > 3) {

			if(PlayerPrefs.GetString("language") == "Portuguese"){

				myImage.sprite = buttonDownloadingPT;

			}else{

				myImage.sprite = buttonDownloadingUK;

			}




			int size = SelectObjectsToSendManagerSistemasNavarra.listOfLinksToSend.Count;
			
			
			if (SelectObjectsToSendManagerSistemasNavarra.listOfLinksToSend.Count > 0) {
				
				
				//    myImage.sprite = buttonDownloading;
				//subject = Lman.getString("assunto_completo");
				
				MailMessage mail = new MailMessage();
				mail.IsBodyHtml = true;
				
				//COM SPAM, É PRECISO MUDAR O LINK DA IMAGEM
				messageBody = "<meta charset=\"utf-8\"/><center><table style=\"width:700px;font-family:Arial,sans-serif\"><tr><td><a href=\"http://www.navarraaluminio.com/pt/grupo-industrial/perfil-1-parceiros-cliente/informacoes-perfis/documentos.html\" target=\"_blank\"><img border=\"0\" height =\"244\" src =\"https://s29.postimg.org/onf3kblmv/image005.jpg\" width=\"700\"></a></td></tr><tr><td style =\"height:20px\"></td></tr><tr><td style =\"background-color:#CCCCCC;text-align:left;padding:20px;margin:30px;font-family:font-family:Arial,sans-serif;font-size:12pt\">";
				
				//SEM SPAM
				//  messageBody = "<meta charset=\"utf-8\"/><center><table style=\"width:700px;font-family:Arial,sans-serif\"><tr><td style =\"background-color:#CCCCCC;text-align:left;padding:20px;margin:30px;font-family:font-family:Arial,sans-serif;font-size:12pt\">";
				
				for (int i = 0; i < size; i++) {
					
					string NomeLista = SelectObjectsToSendManagerSistemasNavarra.listOfLinksToSend[i];
					string tipoficheiro = "";
					
					if (NomeLista.Contains("PDF") == true) {
						tipoficheiro = "PDF";
						NomeLista = NomeLista.Replace("PDF", "");
					} 
					
					if (NomeLista.Contains("BIM") == true) {
						tipoficheiro = "BIM";
						NomeLista = NomeLista.Replace("BIM", "");
					}
					
					if (NomeLista.Contains("DXF") == true) {
						tipoficheiro = "DWG";
						NomeLista = NomeLista.Replace("DXF", "");
					}
					
					messageBody = messageBody + "Download <b>" + tipoficheiro +": ";
					messageBody = messageBody + "<a href=\"" + Lman.getString(SelectObjectsToSendManagerSistemasNavarra.listOfLinksToSend[i]) + "\">"; 
					messageBody = messageBody + NomeLista + "</b></a><br/>" + "<br/>";
				}
				
				messageBody = messageBody + "</tr><tr><td style=\"height:20px\"></td></tr><tr><td style=\"font-family:Arial,sans-serif;font-size:8.5pt;color:white;background-color:#2D9FB7;text-align:left;padding:20px\"><b> INFORMAÇÕES ADICIONAIS </b><br/><b> ADDITIONAL INFORMATION </b><br/><a href=\"mailto:geral@navarraaluminio.com\"> geral@navarraaluminio.com </a><br/> <a href=\"http://www.navarraaluminio.com\" target =\"_blank\"> www.navarraaluminio.com </a><br/><br/> <b> Navarra – Extrusão de Alumínio, S.A.</b><br/>Veiga das Antas, Navarra<br/>Apartado 2476 <br/> 4701 - 971 Braga <br/>t: <a href= \"tel:+351 253 603 520\" target=\"_blank\"> +351 253 603 520 </a><br/> f: <a href=\"tel:+351 253 677 005\" target =\"_blank\"> +351 253 677 005 </a><br/></td></tr><tr><td style =\"height:20px\"></td></tr><tr><td style =\"text-align:right;background-color:#E1E1E1;padding:10px;font-size:8.5pt;color:#9A9A9A\">© 2016 Navarra | todos os direitos reservados | all rights reserved</td></tr></table></center>";
				
				
				mail.From = new MailAddress("web@navarraaluminio.com");
				mail.To.Add(PlayerPrefs.GetString("Email"));
				mail.Subject = Lman.getString("email_subject");
				
				mail.Body = messageBody;
				
				SmtpClient smtpServer = new SmtpClient();
				smtpServer.Host = "62.28.224.83";
				smtpServer.Port = 2525;
				
				
				smtpServer.Credentials = new System.Net.NetworkCredential(originEmail, password) as ICredentialsByHost;
				smtpServer.EnableSsl = true;
				
				smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
				
				ServicePointManager.ServerCertificateValidationCallback =
				delegate (object s, X509Certificate certificate, X509Chain Chain, SslPolicyErrors sslPolicyErrors) { return true; };
				
				ciclo = true;
				
				smtpServer.SendCompleted += new SendCompletedEventHandler(SendCompleteCallback);
				
				
				Debug.Log("Sending Email");
				smtpServer.SendAsync(mail, "byARToken");
				//  smtpServer.Send(mail);
				
				messageBody = "";
			//	StartCoroutine (sendResponseToUser());
				//      myImage.sprite = buttonNotDownloading;
				
			//	while (ciclo == true) {
			//	}
				
			//	if (emailActive) {
			//		Debug.Log("Email Activo");
			//		emailSuccess.SetActive(true);
			//		
					// emailActive = false;
					
			//	} else {
			//		Debug.Log("Email Desactivado");
			//		emailFailed.SetActive(true);
			//	}
				
				//   emailSuccess.SetActive(true);
				
			} else {
				
				// need to add at least one image to email
				needToAddItem.SetActive(true);
				
			}
			
			
		} else {
			
			emailIsWrong.SetActive(true);
			
		}
		
	}

	void Update(){

		if(ciclo == false){


			if(PlayerPrefs.GetString("language") == "Portuguese"){

				myImage.sprite = buttonNotDownloadingPT;

			}else{

				myImage.sprite = buttonNotDownloadingUK;

			}




			if (emailActive) {
				Debug.Log("Email Activo");
				emailSuccess.SetActive(true);

				// emailActive = false;

			} else {
				Debug.Log("Email Desactivado");
				emailFailed.SetActive(true);
			}

			ciclo = true;
		}

	}	

	IEnumerator sendResponseToUser(){


		yield return new WaitForSeconds (7);



		if(PlayerPrefs.GetString("language") == "Portuguese"){

			myImage.sprite = buttonNotDownloadingPT;

		}else{

			myImage.sprite = buttonNotDownloadingUK;

		}



		if (emailActive) {
			Debug.Log("Email Activo");
			emailSuccess.SetActive(true);

			// emailActive = false;

		} else {
			Debug.Log("Email Desactivado");
			emailFailed.SetActive(true);
		}

		//   emailSuccess.SetActive(true);



	}	

	
}






