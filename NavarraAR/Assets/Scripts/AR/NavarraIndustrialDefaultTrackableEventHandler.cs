﻿

using System;
using System.Collections;
using UnityEngine;

namespace Vuforia {
	/// <summary>
	/// A custom handler that implements the ITrackableEventHandler interface.
	/// </summary>
	public class NavarraIndustrialDefaultTrackableEventHandler : MonoBehaviour,
	ITrackableEventHandler {
		#region PRIVATE_MEMBER_VARIABLES

		private TrackableBehaviour mTrackableBehaviour;
		private bool isInstantiated;

		#endregion // PRIVATE_MEMBER_VARIABLES

		public GameObject Loading;
		public GameObject child;
		public GameObject leftPanelARQ;
		public GameObject leftPanelIND;

		public Vector3 pos;
		public Vector3 rot;
		public Vector3 scale;

		public RuntimeAnimatorController AnimControllerParameter;

		#region UNTIY_MONOBEHAVIOUR_METHODS

		void Awake() {

			// seems if I do this then OnTrackableStateChanged will only run OnTrackingLost during this scene and not as a result 
			// of this scene unloading to another one
			// stops the unable to set hint, deinitialise tracker etc and allows AR to continue restarting between scenes
			PlayerPrefs.SetInt("LoadingLevelI", 0);

		}

		void Start() {

			isInstantiated = false;

			EventManager.Instance.AddListener(EVENT_TYPE.ON_TRACKING_LOST, OnEvent);


			mTrackableBehaviour = GetComponent<TrackableBehaviour>();
			if (mTrackableBehaviour) {
				mTrackableBehaviour.RegisterTrackableEventHandler(this);
			}




		}

		private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
			switch (Event_Type) {
			case EVENT_TYPE.ON_TRACKING_LOST:
				OnTrackingLost();

				StartCoroutine(FoundImageTarget());

				break;
			}


		}

		private IEnumerator FoundImageTarget() {
			yield return new WaitForSeconds(1);
			OnTrackingFound();
		}


		#endregion // UNTIY_MONOBEHAVIOUR_METHODS


		#region PUBLIC_METHODS

		/// <summary>
		/// Implementation of the ITrackableEventHandler function called when the
		/// tracking state changes.
		/// </summary>
		public void OnTrackableStateChanged(
			TrackableBehaviour.Status previousStatus,
			TrackableBehaviour.Status newStatus) {
			if (newStatus == TrackableBehaviour.Status.DETECTED ||
				newStatus == TrackableBehaviour.Status.TRACKED ||
				newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
				OnTrackingFound();
			} else {
				if (PlayerPrefs.GetInt("LoadingLevelI") == 0) {
					OnTrackingLost();
				}
			}
		}

		#endregion // PUBLIC_METHODS



		#region PRIVATE_METHODS



		private void OnTrackingFound() {

			Loading.SetActive(true);


			// Activate Child object

			// if (!flag) {
			//   Debug.Log("OUT");



			//} else {


			leftPanelIND.SetActive(true);
			leftPanelARQ.SetActive(false);


			EventManager.Instance.PostNotification(EVENT_TYPE.RESET_STOP_TIME_BUTTON, this);

			StartCoroutine(Load3DModel());


		}

		private IEnumerator Load3DModel() {

			yield return new WaitForSeconds(1f);

			//for (int i = 0; i < child.Length; i++) {

			//    child[i].SetActive(true);
			//}

			child.SetActive(true);

			if (!isInstantiated) {

				GameObject childModel = Instantiate(Resources.Load("RealidadeAumentada/INDUSTRIA_MODELS_NEW/" + gameObject.name + "/" + gameObject.name)) as GameObject;

				string childModelName = childModel.name.Replace("(Clone)", "");
				childModel.name = childModelName;
				childModel.transform.SetParent(child.transform, false);
				childModel.tag = "perfisModel";

			//	childModel.transform.localPosition = pos;
			//	childModel.transform.eulerAngles = rot;
			//	childModel.transform.localScale = scale;

				//  Animator anim = childModel.GetComponent<Animator>();
				//  anim.runtimeAnimatorController = AnimControllerParameter;

				StartCoroutine(StartTheFade(childModel));


				isInstantiated = true;

			}

			Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
			Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);


			//Start Loading Animation


			// Enable rendering:
			foreach (Renderer component in rendererComponents) {
				component.enabled = true;
			}

			// Enable colliders:
			foreach (Collider component in colliderComponents) {
				component.enabled = true;
			}

			EventManager.Instance.PostNotification(EVENT_TYPE.ZWRITE, this);
			//End Loading Animation

			//    Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");


			Loading.SetActive(false);

		//	StartCoroutine(WaitToInitialize());

		}

		private IEnumerator StartTheFade(GameObject child) {
			yield return new WaitForSeconds(.4f);
			child.SendMessage("StartingFade");
		}

		private IEnumerator WaitToInitialize() {

			yield return new WaitForSeconds(.1f);
			//   EventManager.Instance.PostNotification(EVENT_TYPE.UPDATE_BUTTONS_INFO, this);

			//         EventManager.Instance.PostNotification(EVENT_TYPE.FADE_ANIM, this);
		}

		private void OnTrackingLost() {

			Time.timeScale = 1;
			StopTime.timeIsStopped = false;

			//for (int i = 0; i < child.Length; i++) {

			//    child[i].SetActive(false);
			//}

			GameObject[] models = GameObject.FindGameObjectsWithTag("perfisModel");

			for (int i = 0; i < models.Length; i++) {

				if (models[i] != null)
					DestroyImmediate(models[i]);
			}




			//    EventManager.Instance.RemoveRedundancies();
			// EventManager.Instance.PostNotification(EVENT_TYPE.DESTROY_SELF,this);

			isInstantiated = false;
			child.SetActive(false);

			leftPanelIND.SetActive(false);

			Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
			Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

			// Disable rendering:
			foreach (Renderer component in rendererComponents) {
				component.enabled = false;
			}

			// Disable colliders:
			foreach (Collider component in colliderComponents) {
				component.enabled = false;
			}


			// Deactivate Child object


			//   Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
		}




		#endregion // PRIVATE_METHODS
	}


}

