﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class SistemasNavarraVerObra : MonoBehaviour {

    public GameObject parent;    
    public string verObraScene;
    public string nextScene;
    public Image myImage;
    public static bool flagVerUmaObra = false;

    private SistemasNavarraContentPDFBIMDXF sistNavarraContent;

    private ChangeLanguageKeyOfDescriptionPortfolioDeObras changeLangKeyOf;
    private string fileLocation;
    private Lang Lman;
    private string dataPath;
    private string[] keys = { "titulo", "gabinete", "localizacao", "sistemas", "tipologia", "serralharia", "dono_da_obra" };

    private Image myBackground;

    void Awake() {
        dataPath = Application.streamingAssetsPath;
    }


    void Start() {
         

        sistNavarraContent = parent.GetComponent<SistemasNavarraContentPDFBIMDXF>();
        

        // Se existe , icone branco
        if (sistNavarraContent.hasProjectAssociated) {

            myImage.color = new Color(255, 255, 255, 1);

        } else {
            
        // Se não existe, icone cinzento

            myImage.color = new Color(255, 255, 255, .3019f);
        }

        myBackground = GameObject.FindGameObjectWithTag("myBackground").GetComponent<Image>();
         
    }


    public void VerObra() {

        GameObject[] toggleImages = Resources.LoadAll<GameObject>("PortfolioDeObrasAUX/Obras/" + parent.name);

        // Se existe, criar logica de ir para o Ver Obra
        if (toggleImages.Length == 1) {

            flagVerUmaObra = true;

            changeLangKeyOf = toggleImages[0].GetComponent<ChangeLanguageKeyOfDescriptionPortfolioDeObras>();

            fileLocation = changeLangKeyOf.fileLocation;

            Lman = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation, false);

            PlayerPrefs.SetString("gabineteObject", Lman.getString(keys[1]));
            PlayerPrefs.SetString("localizacaoObject", Lman.getString(keys[2]));
            PlayerPrefs.SetString("sistemasObject", Lman.getString(keys[3]));
            PlayerPrefs.SetString("tipologiaObject", Lman.getString(keys[4]));
            PlayerPrefs.SetString("serralhariaObject", Lman.getString(keys[5]));
            PlayerPrefs.SetString("dono_da_obraObject", Lman.getString(keys[6]));
            PlayerPrefs.SetString("obraTitleName", Lman.getString(keys[0]));

            PlayerPrefs.SetString("obra", "");

            PlayerPrefs.SetString("perfilAluminio", parent.name);

            PlayerPrefs.SetString("loadPortfolioDeObras", parent.name);


            // Fade Out
            myBackground.CrossFadeAlpha(1.0f, .5f, false);

            StartCoroutine(VerUmaObraHome());
             
          


        } else if (toggleImages.Length > 1) {


            PlayerPrefs.SetString("loadPortfolioDeObras", parent.name);
            //   PlayerPrefs.SetString("obra", "");
            flagVerUmaObra = false;

            myBackground.CrossFadeAlpha(1.0f, .5f, false);
            // Fade Out

            StartCoroutine(VerVariasObras());

           



        } else {

            //File does not contain images
            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_MARKER_INFO_NO_OBRAS, this);

       
             
        }

    }

    private IEnumerator VerUmaObraHome() {

        yield return new WaitForSeconds(.5f);

        SceneManager.LoadScene("VerObraV2");
    }

    private IEnumerator VerVariasObras() {

        yield return new WaitForSeconds(.5f);

        SceneManager.LoadScene(nextScene);
    }


}

 

 
