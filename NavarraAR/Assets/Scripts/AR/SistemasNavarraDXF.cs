﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SistemasNavarraDXF : MonoBehaviour {

    public GameObject parent;
    public Image myImage;
    public Sprite selectedButton;
    public Sprite notSelectedButton;

    private SistemasNavarraContentPDFBIMDXF sist;
   
    
    private bool itemSelected;

    private string parentNameDXF;

    public string ParentNamePDF {
        get {
            return parentNameDXF;
        }
        set {
            parentNameDXF = value;
        }
    }

    // Use this for initialization
    void Start() {

        parentNameDXF = parent.name + "DXF";
        parentNameDXF = parentNameDXF.Replace("(Clone)", "");

        itemSelected = false;

        sist = parent.GetComponent<SistemasNavarraContentPDFBIMDXF>();
       


        if (sist.hasDXF) {

            // color white

            myImage.color = new Color(255, 255, 255, 1);

        } else {

            // color grey
            myImage.color = new Color(255, 255, 255, .3019f);
        }


    }

    public void OnclickDXF() {

        if (sist.hasDXF && PlayerPrefs.HasKey("unlockFiles")) {

            // if the item is not selected, send an Event to the Manager, to add it to the List.
            // If the item is selected, send an Event to the Manager, to remove it from the List.

            if (!itemSelected) {

                myImage.sprite = selectedButton;

                EventManager.Instance.PostNotification(EVENT_TYPE.ADD_ITEM_TO_SISTEMAS_NAVARRA, this, parentNameDXF);

            } else {

                myImage.sprite = notSelectedButton;

                EventManager.Instance.PostNotification(EVENT_TYPE.REMOVE_ITEM_FROM_SISTEMAS_NAVARRA, this, parentNameDXF);

            }

            itemSelected = !itemSelected;


        } else if (!sist.hasDXF) {

            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_MARKER_INFO_NO_FILE, this);

        } else if (!PlayerPrefs.HasKey("unlockFiles")) {

            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_MARKER_INFO_SISTEMAS_NAVARRA, this);

        }
    }
}
