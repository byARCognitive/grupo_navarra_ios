﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActivateMarkerInfo : MonoBehaviour {

    public GameObject needSpecialMarker;
    public GameObject noFile;
    public GameObject noProjectsAssociated;
    public GameObject noProfilesAssociated;
    public GameObject panelWithInfo;

    private SistemasNavarraVerFichadePerfil fichaPerfil;
    private Sprite mySprite;

    public Image panelWithInfoImage;

    void Start() {

        EventManager.Instance.AddListener(EVENT_TYPE.SHOW_MARKER_INFO_SISTEMAS_NAVARRA, OnEvent);
        EventManager.Instance.AddListener(EVENT_TYPE.SHOW_MARKER_INFO_NO_FILE, OnEvent);
        EventManager.Instance.AddListener(EVENT_TYPE.SHOW_MARKER_INFO_NO_OBRAS, OnEvent);
        EventManager.Instance.AddListener(EVENT_TYPE.SHOW_NO_PROFILE_IMAGE, OnEvent);
        EventManager.Instance.AddListener(EVENT_TYPE.SHOW_PROFILE_IMAGE, OnEvent);
    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {

        switch (Event_Type) {
            case EVENT_TYPE.SHOW_MARKER_INFO_SISTEMAS_NAVARRA:
                needSpecialMarker.SetActive(true);
                break;
            case EVENT_TYPE.SHOW_MARKER_INFO_NO_FILE:
                noFile.SetActive(true);
                break;
            case EVENT_TYPE.SHOW_MARKER_INFO_NO_OBRAS:
                noProjectsAssociated.SetActive(true);
                break;
            case EVENT_TYPE.SHOW_NO_PROFILE_IMAGE:
                noProfilesAssociated.SetActive(true);
                break;
            case EVENT_TYPE.SHOW_PROFILE_IMAGE:
                 
                

                panelWithInfo.SetActive(true);

                fichaPerfil = Sender.GetComponent<SistemasNavarraVerFichadePerfil>();
                
                mySprite = Resources.Load<Sprite>("ProfilesSistemasNavarra/" + fichaPerfil.ParentNameProfile);
               
                panelWithInfoImage.sprite = mySprite;
                break;
        }

    }
}
