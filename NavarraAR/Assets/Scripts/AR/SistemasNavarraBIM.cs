﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SistemasNavarraBIM : MonoBehaviour {

    public GameObject parent;
    public Sprite selectedButton;
    public Sprite notSelectedButton;
    public Image myImage;

    private SistemasNavarraContentPDFBIMDXF sist;
   
    private string dataPath;


    private bool itemSelected;

    private string parentNameBIM;

    public string ParentNameBIM {
        get {
            return parentNameBIM;
        }
        set {
            parentNameBIM = value;
        }
    }



    void Awake() {

        //if (Application.platform == RuntimePlatform.Android) {
        //    //   dataPath = "jar:file://" + Application.dataPath + "!/assets/";
        //    //
        //    dataPath = Application.dataPath;
        //} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
        //    dataPath = Application.streamingAssetsPath;
        //}

        dataPath = Application.streamingAssetsPath;

    }



    // Use this for initialization
    void Start() {

        parentNameBIM = parent.name + "BIM";
        parentNameBIM = parentNameBIM.Replace("(Clone)", "");

        itemSelected = false;

        sist = parent.GetComponent<SistemasNavarraContentPDFBIMDXF>();
       



        if (sist.hasBIM) {

            // color white

            myImage.color = new Color(255, 255, 255, 1);

        } else {

            // color grey
            myImage.color = new Color(255, 255, 255, .3019f);
        }


    }

    public void OnclickBIM() {

        if (sist.hasBIM && PlayerPrefs.HasKey("unlockFiles")) {

            // if the item is not selected, send an Event to the Manager, to add it to the List.
            // If the item is selected, send an Event to the Manager, to remove it from the List.

            if (!itemSelected) {

                myImage.sprite = selectedButton;

                EventManager.Instance.PostNotification(EVENT_TYPE.ADD_ITEM_TO_SISTEMAS_NAVARRA, this, parentNameBIM);

            } else {

                myImage.sprite = notSelectedButton;

                EventManager.Instance.PostNotification(EVENT_TYPE.REMOVE_ITEM_FROM_SISTEMAS_NAVARRA, this, parentNameBIM);

            }

            itemSelected = !itemSelected;


        } else if (!sist.hasBIM) {

            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_MARKER_INFO_NO_FILE, this);

        } else if (!PlayerPrefs.HasKey("unlockFiles")) {

            EventManager.Instance.PostNotification(EVENT_TYPE.SHOW_MARKER_INFO_SISTEMAS_NAVARRA, this);

        }
         

        }
    }
 