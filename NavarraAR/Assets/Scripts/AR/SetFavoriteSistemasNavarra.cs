﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetFavoriteSistemasNavarra : MonoBehaviour {

    public GameObject parent;
    public Image myImage;
    private SistemasNavarraContentPDFBIMDXF sistNavarraContent;
    private GameObject objectInResources;
    private string fileName;
    private int preferences;

    // Use this for initialization
    void Start () {

        fileName = parent.name;
        // sistNavarraContent = parent.GetComponent<SistemasNavarraContentPDFBIMDXF>();

        objectInResources = Resources.Load<GameObject>("ProdutosSistemaNavarra/" + fileName);

        sistNavarraContent = objectInResources.GetComponentInChildren<SistemasNavarraContentPDFBIMDXF>();
          
    }


    public void DrawFavoriteStar() {

        preferences = PlayerPrefs.GetInt( fileName + "dataSistNav", 0);

        if (preferences == 1) {
            Debug.Log("isnot");
            // if it's favorite, stops being favorite
            PlayerPrefs.SetInt(fileName + "dataSistNav", 0);
            myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 0);

        }else {
            Debug.Log("isFAVORITE");
            // if it's not favorite, starts being favorite
            PlayerPrefs.SetInt(fileName + "dataSistNav", 1);
            myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 1);
        }
        //if (sistNavarraContent.isFavorite) {
        //    Debug.Log("isnot");
        //    // if it's favorite, stops being favorite
        //    sistNavarraContent.isFavorite = false;
        //    myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 0);

        //} else {
        //    Debug.Log("isFAVORITE");
        //    // if it's not favorite, starts being favorite
        //    sistNavarraContent.isFavorite = true;
        //    myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 1);

        //}
    }


}
