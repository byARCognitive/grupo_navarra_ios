﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class GetFavoriteInfoSistemasNavarra : MonoBehaviour {

 
    public GameObject parent;
    private Image myImage;
    private Color myColor;
    private SistemasNavarraContentPDFBIMDXF sistNavarraContent;
    private GameObject objectInResources;
    private string fileName;
    private int preferences;
    // Use this for initialization
    void Start () {


        myImage = gameObject.GetComponent<Image>();

        fileName = parent.name;
        objectInResources = Resources.Load<GameObject>("ProdutosSistemaNavarra/" + fileName);

        sistNavarraContent = objectInResources.GetComponentInChildren<SistemasNavarraContentPDFBIMDXF>();

        preferences = PlayerPrefs.GetInt(fileName + "dataSistNav", 0);

        DrawFavoriteStar();
         
    }
 

    private void DrawFavoriteStar() {

        if (preferences == 1) {
            myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 1);
        }else {
            myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 0);
        }

        //if (sistNavarraContent.isFavorite) {
        //    Debug.Log("FAVORITE");
        //    myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 1);
             

        //} else {
        //    Debug.Log("NOT");
        //    myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, 0);
            

        //}

    }
 
}
