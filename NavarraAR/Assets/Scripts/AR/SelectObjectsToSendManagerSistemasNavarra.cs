﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class SelectObjectsToSendManagerSistemasNavarra : MonoBehaviour {

    public static List<string> listOfLinksToSend;
    public Text numberOfLinksText;

    private int numberOfLinks;

	// Use this for initialization
	void Start () {

        EventManager.Instance.AddListener(EVENT_TYPE.ADD_ITEM_TO_SISTEMAS_NAVARRA, OnEvent);
        EventManager.Instance.AddListener(EVENT_TYPE.REMOVE_ITEM_FROM_SISTEMAS_NAVARRA, OnEvent);              

        listOfLinksToSend = new List<string>();
        numberOfLinks = listOfLinksToSend.Count;
        numberOfLinksText.text = numberOfLinks.ToString();

    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
        switch (Event_Type) {

            case  EVENT_TYPE.ADD_ITEM_TO_SISTEMAS_NAVARRA:

                listOfLinksToSend.Add( (string) Param);

                ChangeNumberOfLinks(1);
               

                break;
            case EVENT_TYPE.REMOVE_ITEM_FROM_SISTEMAS_NAVARRA:
                 
                listOfLinksToSend.Remove( (string) Param );
 
                ChangeNumberOfLinks(-1);

                break;

        }

        numberOfLinksText.text = numberOfLinks.ToString();


    }


    private void ChangeNumberOfLinks(int numb) {
        numberOfLinks = numberOfLinks + numb;
    }

     
}
