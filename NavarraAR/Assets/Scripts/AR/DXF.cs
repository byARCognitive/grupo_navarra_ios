﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.UI;

public class DXF : MonoBehaviour {

    private NavarraArquitecturaDefaultTrackableEventHandler arquitectureDefaultTracker;
    private string dataPath;
    private string dxfFileName;
    private UnityEngine.UI.Image myImage;

 

    public string DXFFileName {
        get {
            return dxfFileName;
        }
        set {
            dxfFileName = value;
        }
    }
    // Use this for initialization

    public Sprite mySprite;
    public GameObject fileDoesNotExist;
    public GameObject needToActivateSpecialMarker;

    void Awake() {
        dataPath = Application.streamingAssetsPath;
    }


    void Start() {

        myImage = gameObject.GetComponent<UnityEngine.UI.Image>();

        EventManager.Instance.AddListener(EVENT_TYPE.UPDATE_BUTTONS_INFO, OnEvent);

    }
     

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {

        switch (Event_Type) {

            case EVENT_TYPE.UPDATE_BUTTONS_INFO:

                arquitectureDefaultTracker = Sender.GetComponent<NavarraArquitecturaDefaultTrackableEventHandler>();
                DXFFileName = arquitectureDefaultTracker.DXFName;
                //Check dxf Property

                if (arquitectureDefaultTracker.Hasdxf) {

                    //If true, color white
                    myImage.color = new Color(255, 255, 255, 1);

                } else {

                    //If false, color grey
                    
                    myImage.color = new Color(255, 255, 255, .3019f);
                }
                
                 
                break;

        }


    }


    public void OndxfClick() {

      

        if (arquitectureDefaultTracker.Hasdxf && NavarraFileUnblocker.unlockFiles) {

            //If true, download file and change image to bright
            myImage.sprite = mySprite;

            StartCoroutine(WaitTime());

            

        } else if (!arquitectureDefaultTracker.Hasdxf) {

            //If false, a pop up appears saying that there is no BIM
            fileDoesNotExist.SetActive(true);

            

        } else if (!NavarraFileUnblocker.unlockFiles) {

            //If false, a pop up appears saying that there is no BIM
            needToActivateSpecialMarker.SetActive(true);

             

        }
    }

    private IEnumerator WaitTime() {

        yield return new WaitForSeconds(.01f);
        EventManager.Instance.PostNotification(EVENT_TYPE.SEND_DXF_TO_EMAIL, this);


    }
}
