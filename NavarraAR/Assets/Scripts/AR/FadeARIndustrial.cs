﻿using UnityEngine;
using System.Collections;
using System;

public class FadeARIndustrial : MonoBehaviour {
	
	public Material[] materialToFade;
	public Material[] materialToOpaque;
	public Material[] materialToTransparent;
	
	
	public float timeToWaitBeforeFade;
	private bool startFade = false;
	private bool endAnimation = false;
	private Renderer[] renderer;
	private float timeLeft = 0;
	public float animationDuration;
	private Animator animator;
	public bool Ciclo = false;
	
	// Use this for initialization
	void Start () {
		
		animator = gameObject.GetComponent<Animator>();
		
		
		renderer = gameObject.GetComponentsInChildren<Renderer>();
		
		
		startFade = false;
		//     EventManager.Instance.AddListener(EVENT_TYPE.FADE_ANIM, OnEvent);
		//     EventManager.Instance.AddListener(EVENT_TYPE.DESTROY_SELF, OnEvent);
		
	}
	
	
	// Update is called once per frame
	void Update () {
		
		if (startFade) {
			
			
			timeLeft += Time.deltaTime;
			
			FadeWithRenderer(1, 0.25f, timeLeft, renderer);
			
			//for (int i = 0; i < materialToFade.Length; i++) {
			
			//    materialToFade[i].color = new Color(materialToFade[i].color.r, materialToFade[i].color.g, materialToFade[i].color.b, Mathf.Lerp(1, 0.5f, timeLeft));
			//}
			
			
			if (timeLeft > 1f) {
				
				startFade = false;
				endAnimation = true;
				
			}
		}
		
		if (endAnimation) {
			
			timeLeft += Time.deltaTime;
			
			// Se estiver na ultima animação, desligo o gameObject
			
			if(timeLeft > animationDuration) {
				FadeWithRenderer(1, 1, 1, renderer);
				timeLeft = 0;
				endAnimation = false;
				
				//     EventManager.Instance.PostNotification(EVENT_TYPE.FADE_ANIM,this);
				MakeOpaque(materialToOpaque);
				
				animator.SetTrigger("startAnim");
				StartingFade();
				//           EventManager.Instance.PostNotification(EVENT_TYPE.ON_TRACKING_LOST,this);
			}
			
		}
		
	}
	
	// Fade Event
	//private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
	//    switch (Event_Type) {
	//        case EVENT_TYPE.FADE_ANIM:
	
	//                FadeWithRenderer(1, 1, 1, renderer);
	
	//                timeLeft = 0;
	//                endAnimation = false;
	
	//                startFade = false;
	
	
	//            if (gameObject.activeSelf) {
	//                // Esperar x segundos para começar o fade.
	
	//                StartCoroutine(StartFade());
	
	//                }
	
	//            break;
	//        case EVENT_TYPE.DESTROY_SELF:
	//            EventManager.Instance.RemoveRedundancies();
	
	//            Destroy(gameObject);
	
	//            break;
	//    }
	//}
	
	
	public void StartingFade() {
		
		FadeWithRenderer(1, 1, 1, renderer);
		
		timeLeft = 0;
		endAnimation = false;
		
		startFade = false;
		
		
		if (gameObject.activeSelf) {
			// Esperar x segundos para começar o fade.
			
			StartCoroutine(StartFade());
			
		}
	}
	
	// Start Fade Out Materials
	private IEnumerator StartFade() {
		
		yield return new WaitForSeconds(timeToWaitBeforeFade);
		
		MakeFade(materialToFade);
		
		
		yield return new WaitForSeconds(1);
		startFade = true;
		
	}
	
	
	// Fade Animation With Renderer
	private void FadeWithRenderer(float valueFrom, float valueTo, float timeLeft, Renderer[] render) {
		
		foreach (Renderer rend in render) {
			
			if(rend != null)
				
			foreach (Material mat in rend.materials) {
				
				//mat.name.Replace(" (Instance)", "");
				
				//Debug.Log(mat.name);
				//     if (Ciclo == false) 
				//       {
				
				//if (mat.name !="Barras") {
				//    mat.SetFloat("_Mode", 2);
				//    mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
				//    mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
				//    mat.SetInt("_ZWrite", 0);
				//    mat.DisableKeyword("_ALPHATEST_ON");
				//    mat.EnableKeyword("_ALPHABLEND_ON");
				//    mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
				//    mat.renderQueue = 3000;
				
				if (mat.name == ("Vitres " + "(Instance)")) {
					
					
					//    mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, Mathf.Lerp(.5f, .5f, timeLeft));
				} else {
					mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, Mathf.Lerp(valueFrom, valueTo, timeLeft));
					
				}
				
				//} else {   }
				
				//        }
				
			}
			
		}
		
		Ciclo = true;
		
	}
	
	// Make Materials Fade
	private void MakeFade(Material[] mats) {
		
		for (int i = 0; i < mats.Length; i++) {
			mats[i].SetFloat("_Mode", 2);
			mats[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			mats[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			mats[i].SetInt("_ZWrite", 0);
			mats[i].DisableKeyword("_ALPHATEST_ON");
			mats[i].EnableKeyword("_ALPHABLEND_ON");
			mats[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
			mats[i].renderQueue = 3000;
			
		}
		
		
	}
	
	// Make Materials Opaque
	private void MakeOpaque(Material[] mats) {
		
		for (int i = 0; i < mats.Length; i++) {
			mats[i].SetFloat("_Mode", 0);
			mats[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
			mats[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
			mats[i].SetInt("_ZWrite", 1);
			mats[i].DisableKeyword("_ALPHATEST_ON");
			mats[i].DisableKeyword("_ALPHABLEND_ON");
			mats[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
			mats[i].renderQueue = -1;
			
		}
		
	}
	
	
	private void MakeTransparent(Material[] mats) {
		
		
		for (int i = 0; i < mats.Length; i++) {
			
			mats[i].SetFloat("_Mode", 3);
			mats[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
			mats[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			mats[i].SetInt("_ZWrite", 0);
			mats[i].DisableKeyword("_ALPHATEST_ON");
			mats[i].DisableKeyword("_ALPHABLEND_ON");
			mats[i].EnableKeyword("_ALPHAPREMULTIPLY_ON");
			mats[i].renderQueue = 3000;
			
		}
		
	}
	
	void OnDestroy() {
		Debug.Log("DESTROYEDDDDD");
		EventManager.Instance.RemoveRedundancies();
	}
	
}
