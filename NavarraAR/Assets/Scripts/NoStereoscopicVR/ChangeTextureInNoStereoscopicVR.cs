﻿using UnityEngine;
using System.Collections;

public class ChangeTextureInNoStereoscopicVR : MonoBehaviour {

    private MeshRenderer mesh;
    private Texture text;
	// Use this for initialization
	void Start () {
         
        mesh = GameObject.FindGameObjectWithTag("sphere").GetComponent<MeshRenderer>();

        LoadTexture();

    }

    private void LoadTexture() {

        text = Resources.Load<Texture>("RealidadeVirtual/" + PlayerPrefs.GetString("textureVR"));
        mesh.material.SetTexture("_MainTex" , text);
    }
}
