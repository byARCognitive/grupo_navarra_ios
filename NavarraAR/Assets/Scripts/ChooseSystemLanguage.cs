﻿using UnityEngine;
using System.Collections;

public class ChooseSystemLanguage : MonoBehaviour {

    private string userLanguage;
    public static bool changeLanguageOnce = true;
	// Use this for initialization
	void Start () {

        userLanguage = Application.systemLanguage.ToString();

        ChooseUserLanguage();

	}
	
    private void ChooseUserLanguage() {

        if (changeLanguageOnce) {

            if (userLanguage.Equals("Portuguese")) {

                PlayerPrefs.SetString("language", "Portuguese");

            } else {

                PlayerPrefs.SetString("language", "English");

            }

        }

        changeLanguageOnce = false;




    }
}
