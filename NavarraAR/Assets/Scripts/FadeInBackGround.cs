﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeInBackGround : MonoBehaviour {

    public Image image;

    // Use this for initialization
    void Start() {

        image.canvasRenderer.SetAlpha(0.0f);

        StartCoroutine(Waiting());


    }

    private IEnumerator Waiting() {
        yield return new WaitForSeconds(0);
        image.CrossFadeAlpha(1.0f, 1f, false);
    }
}
