﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

public class SendEmailTest : MonoBehaviour {

    public void Send() {
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("pedro.pereira@byar.pt");
        mail.To.Add("joao.ladeira@byar.pt");
        mail.Subject = "Test Mail";
        mail.Body = "This is for testing SMTP mail from GMAIL";
        
        SmtpClient smtpServer = new SmtpClient("mail.navarra-alu.com");
        smtpServer.Port = 443;
        smtpServer.Credentials = new System.Net.NetworkCredential("web@navarraaluminio.com", "NavarraWEB") as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
         smtpServer.Send(mail);
   //     StartCoroutine(sendEmailToServer("pedro enviou email"));
     //   Debug.Log("success");

    }

    IEnumerator sendEmailToServer(string jsonEmail) {
        WWWForm form = new WWWForm();
        var headers = new System.Collections.Generic.Dictionary<string, string>();
        headers.Add("Content-type", "application/json");

        form.AddField("email", jsonEmail);
        byte[] rawData = Encoding.UTF8.GetBytes(jsonEmail);
        string url = "https://168.63.94.139/sendEmail";
        //  string url = "https://62.28.224.83/sendEmail";

        Debug.Log("Sending request");
        WWW www = new WWW(url, rawData, headers);
        yield return www;
        if (www.error != null) {
            Debug.Log("www error " + www.error);
            yield break;
        } else {
            Debug.Log("Comunication with server happened");
        }
    }

}
