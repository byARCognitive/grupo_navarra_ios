﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChangeButtonImageNoAnim : MonoBehaviour, IPointerDownHandler {
     
 
    public Sprite on;

    private bool slideIsIn;
    private Button button;
 
    private SpriteState sprState;

    void Start() {

        button = gameObject.GetComponent<Button>();
 
        sprState = new SpriteState();
    }


    public void OnPointerDown(PointerEventData eventData) {

        button.gameObject.GetComponent<Image>().sprite = on;
        button.spriteState = sprState;


    }
 
    }
 
