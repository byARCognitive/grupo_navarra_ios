﻿using UnityEngine;
using System.Collections;
using System;

public class MoveTextUpPanelScrollView : MonoBehaviour {

    public Animator anim;
    private Vector3 targetPosition = new Vector3(0,0,0);
    //public Vector3 currentPosition;
    private bool flag;
    private float tempo = 0;
    private float posicao = 0;
    public float smoothFactor = 1;
    private float tempo_ani = 0;
    
	// Use this for initialization
	void Start () {

        flag = false;
        tempo = 0;
        posicao = 0;
        tempo_ani = 0.15f;

        EventManager.Instance.AddListener(EVENT_TYPE.SCROLL_DOWN_ANIMATION, OnEvent);
        
    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
        switch (Event_Type) {
            case EVENT_TYPE.SCROLL_DOWN_ANIMATION:
                flag = false;
                tempo = 0;
                posicao = 0;
                tempo_ani = 0.15f;
                scrollDownAnim();

                break;
        }
    }

    void Update() {
         
        scrollDownAnim();
         
        }

    private void scrollDownAnim() {

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("PerfisTextPanelSlide") && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 && flag == false) {
            flag = true;
            targetPosition = new Vector3(transform.position.x, transform.position.y - 80, transform.position.z);

        }

        if (flag == true && tempo < tempo_ani) {
            tempo += Time.deltaTime;

            posicao = tempo / tempo_ani;
            transform.position = Vector3.Lerp(transform.position, targetPosition, posicao);

        }

    }
      
    }
	
	 
 