﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeImage : MonoBehaviour {

    public Image[] images;
    public float waitToFade;
    public float initialValue;
    public float toValue;
    private float duration;
    public bool destroyAfter;
    // Use this for initialization
    void Start() {

        duration = 0.2f;

        foreach (Image image in images) {
              
            image.canvasRenderer.SetAlpha(initialValue);

        }
         
        StartCoroutine(Waiting());


    }

    private IEnumerator Waiting() {
        yield return new WaitForSeconds(waitToFade);

        foreach (Image image in images) {
            image.CrossFadeAlpha(toValue, duration, false);

        }

        yield return new WaitForSeconds(duration + .1f);

        if (destroyAfter) {

            foreach (Image image in images) {
                image.gameObject.SetActive(false);
            }
            
        }
    }
}
