﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InstantiateOneProjectVerObra : MonoBehaviour {


    public GameObject imageToInstantiate;
    public GameObject toggleToInstantiate;
    public Sprite onlyToggleBackGround;
    public GameObject NextArrow;
    public GameObject PreviousArrow;

    private Image imageComponent;
    private Sprite[] images;

    public GameObject imagesParent;
    public GameObject togglesParent;

    private string obra;
    private string perfilAluminio;

    public GameObject ImagesPanel;
    public GameObject TogglesPanel;
    private GameObject[] imagesToDestroy;
    private GameObject[] togglesToDestroy;

    public static bool cenas;



    // Use this for initialization
    void Awake() {
        cenas = false;
        obra = PlayerPrefs.GetString("obra");
        perfilAluminio = PlayerPrefs.GetString("perfilAluminio");

        InstantiateBackground();

    }

    void Start() {

     //   EventManager.Instance.AddListener(EVENT_TYPE.LOAD_IMAGES_PORTFOLIO_VER_OBRA, OnEvent);
    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
        switch (Event_Type) {
            case EVENT_TYPE.LOAD_IMAGES_PORTFOLIO_VER_OBRA:

                obra = PlayerPrefs.GetString("obra");
                perfilAluminio = PlayerPrefs.GetString("perfilAluminio");
                InstantiateBackground();

                break;
        }

    }

    private void InstantiateBackground() {



        images = Resources.LoadAll<Sprite>("PortfolioDeObrasImagens/Images/" + perfilAluminio + "/" + obra);

        for (int i = 0; i < images.Length; i++) {

            InstantiateBackgroundImages(i);

        }

        for (int i = 0; i < images.Length; i++) {

            InstantiateToggleButtons(i, images.Length);
        }


    }


    private void InstantiateBackgroundImages(int i) {

        GameObject image = Instantiate(imageToInstantiate);

        image.transform.SetParent(imagesParent.transform, false);
        image.tag = "ImagesObras";
        imageComponent = image.GetComponent<Image>();

        imageComponent.sprite = images[i];

    }


    private void InstantiateToggleButtons(int i, int imagesLength) {

        if (imagesLength == 1) {

            NextArrow.SetActive(false);
            PreviousArrow.SetActive(false);

        } else {
            NextArrow.SetActive(true);
            PreviousArrow.SetActive(true);

            GameObject toggle = Instantiate(toggleToInstantiate);
            toggle.tag = "togglesObras";
            toggle.transform.SetParent(togglesParent.transform);
            toggle.transform.localScale = new Vector3(1, 1, 1);

        }

    }
}
