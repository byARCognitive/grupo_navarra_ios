﻿using UnityEngine;
using System.Collections;

public class ZWrite : MonoBehaviour {

    private Renderer[] renderer;

    void Start () {

           EventManager.Instance.AddListener(EVENT_TYPE.ZWRITE, OnEvent);

    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param = null) {
        switch (Event_Type) {
            case EVENT_TYPE.ZWRITE:

                renderer = Sender.gameObject.GetComponentsInChildren<Renderer>();
               
                foreach (Renderer rend in renderer) {                                      
                    
                    foreach (Material mat in rend.materials) {
                        mat.EnableKeyword("_ZWrite");
                        mat.SetInt("_ZWrite", 1);
                    }
  
                }
                  
                break;
        }
    }


}
