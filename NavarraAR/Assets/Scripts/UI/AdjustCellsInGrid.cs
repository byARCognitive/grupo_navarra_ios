﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class AdjustCellsInGrid : MonoBehaviour {

    private Rect leftPanelRect;
   
    public GameObject leftPanel;
    private GridLayoutGroup grid;

    public GameObject loading;
    public GameObject loadingImageCenter;
 
    public Image image;
    // Use this for initialization
    void Start () {
        image.canvasRenderer.SetAlpha(1.0f);
        grid = gameObject.GetComponent<GridLayoutGroup>();

        StartCoroutine(Wait());
         
	}
 

    private IEnumerator Wait() {
         
        yield return new WaitForSeconds(0f);

        leftPanelRect = leftPanel.GetComponent<RectTransform>().rect;

        //grid.cellSize = new Vector2( ((leftPanelRect.width) / 3) - (2 * 0.0162f * leftPanelRect.width) , (leftPanelRect.height) / 5);
        //grid.spacing = new Vector2(0.036694f * leftPanelRect.width, 0);
        grid.cellSize = new Vector2(((leftPanelRect.width) / 3) - 15, (leftPanelRect.height) / 5);
        grid.spacing = new Vector2(10, 10);

        

        StartCoroutine(Waiting());
       

    }

    private IEnumerator Waiting() {
        yield return new WaitForSeconds(1f);
        loading.SetActive(false);
        loadingImageCenter.SetActive(false);
        image.CrossFadeAlpha(0.0f, 1f, false);

 
    }

  
}
