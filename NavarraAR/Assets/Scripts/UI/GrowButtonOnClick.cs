﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GrowButtonOnClick : MonoBehaviour {

    private bool startGrowing;
    private float time;
    private float duration;
    private float position;
    private Vector3 targetScale;

    public string sceneName;
	// Use this for initialization
	void Start () {

        startGrowing = false;
        time = 0;
        duration = .3f;
        targetScale = new Vector3(1.253592f, 1.253592f, 1.253592f);

    }
	
	// Update is called once per frame
	void Update () {

        if (startGrowing) {
           
            if (time < duration) {

                time += Time.deltaTime;
                position = time / duration;

                gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, targetScale, position);

            }else {

                SceneManager.LoadScene(sceneName);
                
            }


        }

	}

    public void OnClickToGrow() {

        startGrowing = true;
       
    }
}
