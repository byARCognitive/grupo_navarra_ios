﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeactivateScreen : MonoBehaviour {

    public GameObject objectToDeactivate;
    private GameObject[] imagesToDestroy;
    private GameObject[] togglesToDestroy;
    public RectTransform Panel;
    public GameObject VerObra;

    //public Animator anim;
    //public AnimationClip animation;

    //public Animator anim2;
    //public AnimationClip animation2;

    public Image image;
    public Image image2;

    public void DeativateScreen() {


        image2.canvasRenderer.SetAlpha(0.0f);
        image2.CrossFadeAlpha(1.0f, .5f, false);

     //   anim.SetTrigger("fadingIn");
         
        StartCoroutine(DestroyAllItems());

        //DestroyAllItems();
        //VerObra.SetActive(true);
        //objectToDeactivate.SetActive(false);

    }

    private IEnumerator DestroyAllItems() {

        yield return new WaitForSeconds(.5f);


        imagesToDestroy = GameObject.FindGameObjectsWithTag("ImagesObras");
        togglesToDestroy = GameObject.FindGameObjectsWithTag("togglesObras");


        foreach (GameObject image in imagesToDestroy) {
            Destroy(image);
        }

        foreach (GameObject toggle in togglesToDestroy) {
            Destroy(toggle);
        }


        Panel.offsetMin = new Vector2(0, 0);

        VerObra.SetActive(true);
       

        image.canvasRenderer.SetAlpha(1.0f);
        image.CrossFadeAlpha(0.0f, .5f, false);
        objectToDeactivate.SetActive(false);
        //    anim2.SetTrigger("fadeIn");
    }


}
