﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AddToggleGroupToTogglePrefab : MonoBehaviour {

    private GameObject gridPanel;

	// Use this for initialization
	void Start () {

        gridPanel = GameObject.FindGameObjectWithTag("GridPanel");
        gameObject.GetComponent<Toggle>().group = gridPanel.GetComponent<ToggleGroup>();

	}
	
 
}
