﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ActivateScreen : MonoBehaviour {

    public GameObject objectToActivate;
    public GameObject VerObra;
    public Animator anim;
    public AnimationClip animation;
 //   public Animator anim2;

    public Image image;
    public Image image2;
    // Use this for initialization
    void Start() {
        
        if (SistemasNavarraVerObra.flagVerUmaObra) {
           ChangeScreen();
       }

    }

    public void ChangeScreen() {

         image.canvasRenderer.SetAlpha(0.0f);
         image.CrossFadeAlpha(1.0f, .5f, false);
  
         StartCoroutine(Waiting());
         
 
    }
     

    private IEnumerator Waiting() {

        yield return new WaitForSeconds(.5f);
        objectToActivate.SetActive(true);

         
     
        EventManager.Instance.PostNotification(EVENT_TYPE.LOAD_IMAGES_PORTFOLIO_VER_OBRA, this);
        EventManager.Instance.PostNotification(EVENT_TYPE.CHANGE_PORTFOLIO_OBRAS_DESCRICAO, this);

        image2.canvasRenderer.SetAlpha(1.0f);
        image2.CrossFadeAlpha(0.0f, .5f, false);
        EventManager.Instance.PostNotification(EVENT_TYPE.SCROLL_DOWN_ANIMATION, this);
        StartCoroutine(Waiting2());
        
    }

    private IEnumerator Waiting2() {

        yield return new WaitForSeconds(.5f);

        VerObra.SetActive(false);
    }
}
