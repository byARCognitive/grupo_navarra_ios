﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeToggleBackgroundCheckmark : MonoBehaviour {

    private GameObject rightPanel;
    private Rect rightPanelRect;

    public GameObject background;
    private RectTransform backgroundRectTransform;

    public GameObject checkmark;
    private RectTransform checkmarkRectTransform;

    // Use this for initialization
    void Start () {
        

        
        StartCoroutine(Wait());
        
    }

    private IEnumerator Wait() {

        yield return new WaitForSeconds(.2f);

        rightPanel = GameObject.FindGameObjectWithTag("LeftPanel");
        rightPanelRect = rightPanel.GetComponent<RectTransform>().rect;

        backgroundRectTransform = background.GetComponent<RectTransform>();
        checkmarkRectTransform = checkmark.GetComponent<RectTransform>();
         
        //backgroundRectTransform.sizeDelta = new Vector2((rightPanelRect.width / 3) - (1 * 0.0162f * rightPanelRect.width), rightPanelRect.height / 5);
        //checkmarkRectTransform.sizeDelta = new Vector2((rightPanelRect.width / 3) - (2 * 0.0162f * rightPanelRect.width), rightPanelRect.height / 5);

        backgroundRectTransform.sizeDelta = new Vector2((rightPanelRect.width / 3) - 15, rightPanelRect.height / 5);
        checkmarkRectTransform.sizeDelta = new Vector2((rightPanelRect.width / 3) - 15, rightPanelRect.height / 5);


    }
}
