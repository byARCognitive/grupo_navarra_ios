﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ChangeIconImagePressedHighlighted : MonoBehaviour {

    public Sprite UKSprite;
    public Sprite PTSprite;
    public Sprite UKSpritePressed;
    public Sprite PTSpritePressed;
 
    private Button myButton;
    private SpriteState sprState;
    private Color myColor;
    private Color myColorObj;
    private bool flag;
    private float tempo;
    private float posicao;
    private float tempo_ani;

    // Use this for initialization
    void Start() {

        tempo_ani = 0.5f;
        flag = false;
        posicao = 0;
        myColorObj.a = 1;

        myButton = gameObject.GetComponent<Button>();
        sprState = new SpriteState();
         
        ChangeImageAccordingToLanguage();
 

    }
   
    private void ChangeImageAccordingToLanguage() {


        myColor = myButton.gameObject.GetComponent<Image>().color;
        myColor.a = 0;
        myColorObj = myColor;
        myColorObj.a = 1;

        if (PlayerPrefs.GetString("language").Equals("Portuguese")) {

            myButton.gameObject.GetComponent<Image>().sprite = PTSprite;
            sprState.pressedSprite = PTSpritePressed;
           // sprState.highlightedSprite = PTSpritePressed;
            myButton.spriteState = sprState;
            flag = true;
             

        } else {

            sprState.pressedSprite = UKSpritePressed;
         //   sprState.highlightedSprite = UKSpritePressed;
         //   myButton.GetComponent<Image>(). = UKSpritePressed;
            myButton.spriteState = sprState;
            myButton.gameObject.GetComponent<Image>().color = myColor;
            myButton.gameObject.GetComponent<Image>().sprite = UKSprite;
           
         
           
            flag = true;
       
        }




    }
    void Update() {


        if (flag == true && tempo <= tempo_ani) {

            tempo += Time.deltaTime;

            posicao = tempo / tempo_ani;
            myButton.gameObject.GetComponent<Image>().color = Color.Lerp(myColor, myColorObj, posicao);

        }
    }
}
