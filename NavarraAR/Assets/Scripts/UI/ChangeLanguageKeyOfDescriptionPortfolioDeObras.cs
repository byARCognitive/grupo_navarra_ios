﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using UnityEngine.UI;

public class ChangeLanguageKeyOfDescriptionPortfolioDeObras : MonoBehaviour {

    private Toggle myToggle;
    private GameObject description;
    public string perfilAluminio;

    private string dataPath;
    private Text textUI;
    private Lang Lman;

    private bool flag;

    public string fileLocation;

    private Text[] infoItems = new Text[7];
    private GameObject[] infoItemsPanelParent = new GameObject[7];

    private string[] keys = {"titulo", "gabinete", "localizacao" , "sistemas" , "tipologia" , "serralharia" , "dono_da_obra"};
 
    void Awake() {
        dataPath = Application.streamingAssetsPath;
    }


    // Use this for initialization
    void Start () {
        flag = true;
        myToggle = gameObject.GetComponent<Toggle>();
        Lman = new Lang(Path.Combine(dataPath, fileLocation), PlayerPrefs.GetString("language"), fileLocation, false);

        infoItems[0] = GameObject.FindGameObjectWithTag("IDPerfil").GetComponent<Text>();
        infoItems[2] = GameObject.FindGameObjectWithTag("Localizacao").GetComponent<Text>();
        infoItems[3] = GameObject.FindGameObjectWithTag("Sistema").GetComponent<Text>();
        infoItems[4] = GameObject.FindGameObjectWithTag("Tipologia").GetComponent<Text>();
        infoItems[1] = GameObject.FindGameObjectWithTag("GabineteProjectista").GetComponent<Text>();
        infoItems[5] = GameObject.FindGameObjectWithTag("Serralharia").GetComponent<Text>();
        infoItems[6] = GameObject.FindGameObjectWithTag("DonaDaObra").GetComponent<Text>();


        infoItemsPanelParent[3] = GameObject.FindGameObjectWithTag("SistemaPanelParent");
        infoItemsPanelParent[4] = GameObject.FindGameObjectWithTag("TipologiaPanelParent");
        infoItemsPanelParent[1] = GameObject.FindGameObjectWithTag("GabineteProjectistaPanelParent");
        infoItemsPanelParent[5] = GameObject.FindGameObjectWithTag("SerralhariaPanelParent");
        infoItemsPanelParent[6] = GameObject.FindGameObjectWithTag("DonoDaObraPanelParent");

         

        if (myToggle.isOn) {
     

            ChangeDescription();
        } 

    }
	
	// Update is called once per frame
	public void ChangeDescription() {
 
		if (myToggle != null && myToggle.isOn) {
             

            if (flag) {

                //Find with Tags
                //infoItems[0] = GameObject.FindGameObjectWithTag("IDPerfil").GetComponent<Text>();
                //infoItems[2] = GameObject.FindGameObjectWithTag("Localizacao").GetComponent<Text>();
                //infoItems[3] = GameObject.FindGameObjectWithTag("Sistema").GetComponent<Text>();
                //infoItems[4] = GameObject.FindGameObjectWithTag("Tipologia").GetComponent<Text>();
                //infoItems[1] = GameObject.FindGameObjectWithTag("GabineteProjectista").GetComponent<Text>();
                //infoItems[5] = GameObject.FindGameObjectWithTag("Serralharia").GetComponent<Text>();
                //infoItems[6] = GameObject.FindGameObjectWithTag("DonaDaObra").GetComponent<Text>();

                flag = false;
            }
            

            // Saves the gameobject name in order to get it from
            PlayerPrefs.SetString("obra", gameObject.name.Replace("(Clone)" ,""));
            PlayerPrefs.SetString("perfilAluminio", perfilAluminio);
             
            for (int i = 0; i < infoItems.Length; i++) {

                if (Lman.getString(keys[i]).Equals("")) {

                    if(i == 0) {
                        infoItems[0].gameObject.SetActive(false);
                    }else if(i == 2) {
                        infoItems[2].gameObject.SetActive(false);
                    }else {

                        infoItemsPanelParent[i].gameObject.SetActive(false);
                    }


                }else {

                    if (i == 0) {
                        infoItems[0].gameObject.SetActive(true);
                    } else if (i == 2) {
                        infoItems[2].gameObject.SetActive(true);
                    } else {
                         
                        infoItemsPanelParent[i].gameObject.SetActive(true);
                    }
                     

                    infoItems[i].text = Lman.getString(keys[i]);
                }

                 
            }

            // Save info of last object pressed

            PlayerPrefs.SetString("gabineteObject", Lman.getString(keys[1]));
            PlayerPrefs.SetString("localizacaoObject", Lman.getString(keys[2]));
            PlayerPrefs.SetString("sistemasObject", Lman.getString(keys[3]));
            PlayerPrefs.SetString("tipologiaObject", Lman.getString(keys[4]));
            PlayerPrefs.SetString("serralhariaObject", Lman.getString(keys[5]));
            PlayerPrefs.SetString("dono_da_obraObject", Lman.getString(keys[6]));
            PlayerPrefs.SetString("obraTitleName", Lman.getString(keys[0]));

         }

    

    }
}
