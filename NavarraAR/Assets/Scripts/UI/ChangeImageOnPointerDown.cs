﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;

public class ChangeImageOnPointerDown : MonoBehaviour, IPointerDownHandler {
    
    public Sprite sprite;
 
    public Image childImage;
    public string screen;
 
 

    public void OnPointerDown(PointerEventData eventData) {
          
        childImage.sprite = sprite;

        if(gameObject.name == "HomeButton") {

            if (PlayerPrefs.GetString("chooseSector").Equals("Industrial")) {

                SceneManager.LoadScene("HomeSceneIndustrialV2");

            } else {

                SceneManager.LoadScene("HomeSceneArquitecturaV2");

            }

        }else {

            SceneManager.LoadScene(screen);
        }
     

    }

     
}
