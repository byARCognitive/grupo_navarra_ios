﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CopyToggleImageToImage : MonoBehaviour {

    private Toggle toggle;
    private Image imageSprite;
 
    private Sprite[] image;
    private string gameObjectName;



	private Image thumbnail;

	// Use this for initialization
	void Start () {

        toggle = gameObject.GetComponent<Toggle>();
        imageSprite = GameObject.FindGameObjectWithTag("ImageHolder").GetComponent<Image>();
        gameObjectName = gameObject.name.Replace("(Clone)","");

        TurnToggleOn();
         
	}
	
	public void TurnToggleOn() {
 

        if(toggle != null) {
            if (toggle.isOn) {

               // image = Resources.LoadAll<Sprite>("PortfolioDeObrasImagens/HighQualityImages/" + gameObjectName);
				thumbnail = gameObject.GetComponentInChildren<Image>();

			 
                // imageSprite.sprite = highqualityImage;
               
          //      imageSprite.sprite = image[0];
				imageSprite.sprite = thumbnail.sprite;
            }
        }

       

    }
}
