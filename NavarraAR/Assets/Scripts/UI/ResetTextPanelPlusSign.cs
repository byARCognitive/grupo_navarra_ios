﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResetTextPanelPlusSign : MonoBehaviour {

    public GameObject topPanelSign;
    public GameObject child;

    public Sprite plusSign;
    public Sprite minusSign;
    private Image myImage;
    private ChangeButtonImageOnTouch changeOnTouch;
    private bool slideIsIn;
    private Image childImage;

    // Use this for initialization
    void Start () {

        changeOnTouch = topPanelSign.GetComponent<ChangeButtonImageOnTouch>();
        slideIsIn = changeOnTouch.slideIsIn;
        childImage = child.GetComponent<Image>();

      //  ChangeButtonImageOnTouch.SlideSim = false;


    }
	
	// Update is called once per frame
	public void ResetSignal() {


        //if (slideIsIn) {

        //    childImage.sprite = plusSign;

        //} else {

        //    childImage.sprite = minusSign;

        //}

        if (ChangeButtonImageOnTouch.SlideSim ) {
            ChangeButtonImageOnTouch.SlideSim = false;
             childImage.sprite = minusSign;
        //    changeOnTouch.anim.SetTrigger("slide");
        }
      
    }
}
