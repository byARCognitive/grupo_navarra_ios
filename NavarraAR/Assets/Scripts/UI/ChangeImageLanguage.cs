﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ChangeImageLanguage : MonoBehaviour {

    public Sprite UKSprite;
    public Sprite PTSprite;
 
    private Button myButton;
    private SpriteState sprState;

    // Use this for initialization
    void Start() {

        myButton = gameObject.GetComponent<Button>();

        ChangeImageAccordingToLanguage();


    }

    private void ChangeImageAccordingToLanguage() {

        if (PlayerPrefs.GetString("language").Equals("Portuguese")) {

            myButton.gameObject.GetComponent<Image>().sprite = PTSprite; 
            myButton.spriteState = sprState;

        } else {

            myButton.gameObject.GetComponent<Image>().sprite = UKSprite; 
            myButton.spriteState = sprState;
        }
    }
}
