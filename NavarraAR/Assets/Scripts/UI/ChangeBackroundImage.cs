﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeBackroundImage : MonoBehaviour {

    public Sprite backgroundIndustrial;
    public Sprite backgroundArquitectura;

	// Use this for initialization
	void Start () {

        if (PlayerPrefs.GetString("chooseSector").Equals("Industrial")) {

            gameObject.GetComponent<Image>().sprite = backgroundIndustrial;

        } else {

            gameObject.GetComponent<Image>().sprite = backgroundArquitectura;
        }
	}
	
	 
}
