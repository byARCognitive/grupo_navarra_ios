﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class FadeOutBackGround : MonoBehaviour {

    public Image image;

	// Use this for initialization
	void Start () {

        image.canvasRenderer.SetAlpha(1.0f);

        StartCoroutine(Waiting());
   
       
    }

    private IEnumerator Waiting() {
        yield return new WaitForSeconds(0.4f);
        image.CrossFadeAlpha(0.0f, 1f, false);
    }

     
}
