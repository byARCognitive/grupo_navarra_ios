﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class industrialCapacityBallsScript : MonoBehaviour {

    private Vector3 initialPosition;
    private Vector3 targetPosition = new Vector3(Screen.width/2,Screen.height/2,0);
    private Vector3 targetTitulo;
    private Vector3 tituloespecial;
    private bool flag;
    private float tempo;
    private float tempo_ani;
    private float posicao;
    private Vector3 escala;
    public GameObject background;
    public GameObject titulo;
    public GameObject TextoObj;
    public GameObject linhas;
    public GameObject button360Obj;
    //  private float textoCor;
    private Button button360;
    private Color textoCor;
    private Color button360Cor;
    private Color button360CorTarget;
    private Color textoCorTarget;
    private bool aumentou = false;
    private bool aumentouFlag = false;
    private int flagAumentou;
    private float initialTitleSize;


    public Animator anim;
    public Sprite off;
    public Sprite on;
    public Image childImage;

    void AnimaCaixaTexto() {

        if (anim != null) {
            if (ChangeButtonImageOnTouch.SlideSim != true) {
                ChangeButtonImageOnTouch.SlideSim = true;
                anim.SetTrigger("slide");
                childImage.sprite = off;

            }

        }


    }


    // Use this for initialization
    void Start () {
        flagAumentou = 0;
        tempo = 0;
        aumentou = false;
        flag = false;
        initialTitleSize = titulo.transform.localPosition.y + 40;
        button360 = button360Obj.GetComponent<Button>();
        button360.interactable = false;

        initialPosition = transform.position;
        textoCor = TextoObj.GetComponent<Text>().color;
        button360Cor = button360Obj.GetComponent<Image>().color;
        TextoObj.GetComponent<Text>().color = new Color(textoCor.r, textoCor.g, textoCor.b, 0);
        button360Obj.GetComponent<Image>().color = new Color(button360Cor.r, button360Cor.g, button360Cor.b, 0);
        EventManager.Instance.AddListener(EVENT_TYPE.IC_SCALE_DOWN, OnEvent);
    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
        switch (Event_Type) {
            case EVENT_TYPE.IC_SCALE_DOWN:
                if (Sender.gameObject.name == gameObject.name) {
                  //  Debug.Log("INITIAL   " + initialTitleSize);
                    //  if(titulo.transform.localPosition.y  >  initialTitleSize + 40)
                 //   Debug.Log(titulo.transform.localPosition.y);
                    ScaleDown();
                    tempo = 0;
                    flag = true;
                    tempo_ani = 1.2f;


                     }
                    break;
                }
        }
    

    // Update is called once per frame
    public void Onclick() {
       
        if (aumentou == false) 
         {
            aumentouFlag = false;
            EventManager.Instance.PostNotification(EVENT_TYPE.INDUSTRIAL_CAPACITY_HIDE_BALLS, this);
            button360.interactable = true;
            ScaleUp();
            
        }
        else 
        {
            aumentouFlag = true;
          //  if (titulo.transform.localPosition.y  >  initialTitleSize-40) {
                Debug.Log(titulo.transform.localPosition.y);
                EventManager.Instance.PostNotification(EVENT_TYPE.IC_DONT_SCALE_DOWN, this);
            button360.interactable = false;
            ScaleDown();
       //     }
        }
      
        tempo = 0;
        flag = true;
        tempo_ani = 1.2f;
      

    }
     

    private void ScaleUp() {



        AnimaCaixaTexto();
        transform.SetAsLastSibling();
        //  linhas.SetActive(false);
        initialPosition = transform.position;
        targetPosition = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        escala = new Vector3(1.5f, 1.5f, 1.5f);
        //  targetTitulo = new Vector3(targetPosition.x, targetPosition.y+70, targetPosition.z);


        //  targetTitulo = new Vector3(titulo.transform.localPosition.x, titulo.transform.localPosition.y + 40, titulo.transform.localPosition.z);
        targetTitulo = new Vector3(titulo.transform.localPosition.x, initialTitleSize + 5, titulo.transform.localPosition.z);
        textoCor = TextoObj.GetComponent<Text>().color;
        textoCorTarget = new Color(textoCor.r, textoCor.g, textoCor.b, 1);
        button360Cor = button360Obj.GetComponent<Image>().color;
        button360CorTarget = new Color(button360Cor.r, button360Cor.g, button360Cor.b, 1);
        aumentou = true;
       
    }

    private void ScaleDown() {
        
        targetPosition = initialPosition;
        escala = new Vector3(1, 1, 1);

        // targetTitulo = new Vector3(titulo.transform.localPosition.x, titulo.transform.localPosition.y - 40, titulo.transform.localPosition.z);
        targetTitulo = new Vector3(titulo.transform.localPosition.x, initialTitleSize - 40, titulo.transform.localPosition.z);

        textoCor = TextoObj.GetComponent<Text>().color;
        textoCorTarget = new Color(textoCor.r, textoCor.g, textoCor.b, 0);
        button360Cor = button360Obj.GetComponent<Image>().color;
        button360CorTarget = new Color(button360Cor.r, button360Cor.g, button360Cor.b, 0);
        aumentou = false;
    }




    void Update() {
 
        if (flag == true && tempo <= tempo_ani) {
           
            tempo += Time.deltaTime;

            posicao = tempo / tempo_ani;
        
          //  transform.position = Vector3.Lerp(transform.position, targetPosition, posicao);
            background.transform.localScale = Vector3.Lerp(background.transform.localScale, escala, posicao);
            titulo.transform.localPosition = Vector3.Lerp(titulo.transform.localPosition, targetTitulo, posicao);
            TextoObj.GetComponent<Text>().color = Color.Lerp (textoCor, textoCorTarget, (posicao+0.5f));
            button360Obj.GetComponent<Image>().color = Color.Lerp(button360Cor, button360CorTarget, (posicao + 0.5f));
        }

        
        

    }
}
