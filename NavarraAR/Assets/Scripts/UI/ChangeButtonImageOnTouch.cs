﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ChangeButtonImageOnTouch : MonoBehaviour, IPointerDownHandler {

    public Animator anim;
    public Sprite off;
    public Sprite on;

    public bool slideIsIn;
    private Button button;
    public Image childImage;
    private SpriteState sprState;
    static public bool SlideSim;

    void Start() {

        button = gameObject.GetComponent<Button>();

        SlideSim = false;
        slideIsIn = false;
        childImage.sprite = off;
        //sprState = new SpriteState();
    }

     
     public void OnPointerDown(PointerEventData eventData) {

        Debug.Log(childImage.sprite);

        anim.SetTrigger("slide");

        if (!SlideSim) {
            SlideSim = false;
            childImage.sprite = on;

        } else {

            childImage.sprite = off;

        }

        SlideSim = !SlideSim;
    }
}
