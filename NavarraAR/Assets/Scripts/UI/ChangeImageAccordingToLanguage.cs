﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeImageAccordingToLanguage : MonoBehaviour {

    public Sprite UKSprite;
    public Sprite PTSprite;

    private Image myImage;
    private SpriteState sprState;

    // Use this for initialization
    void Start() {

        myImage = gameObject.GetComponent<Image>();

        ChangeImageAccordingToLanguages();


    }

    private void ChangeImageAccordingToLanguages() {

        if (PlayerPrefs.GetString("language").Equals("Portuguese")) {
            
            myImage.sprite = PTSprite;

        } else {
           
            myImage.sprite = UKSprite;
        }
    }
}
