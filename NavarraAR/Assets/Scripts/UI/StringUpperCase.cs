﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StringUpperCase : MonoBehaviour {

    private Text myText;

	// Use this for initialization
	void Start () {

        myText = gameObject.GetComponent<Text>();
        myText.text.ToUpper();
    }
	
	// Update is called once per frame
	void Update () {
 
    }
}
