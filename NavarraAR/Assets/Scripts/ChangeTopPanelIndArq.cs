﻿using UnityEngine;
using System.Collections;

public class ChangeTopPanelIndArq : MonoBehaviour {

    public GameObject topPanelARQ;
    public GameObject topPanelIND;

	// Use this for initialization
	void Start () {

        if (PlayerPrefs.GetString("chooseSector").Equals("Industrial")) {

            topPanelIND.SetActive(true);

        } else {

            topPanelARQ.SetActive(true);
        }

    }
	 
}
