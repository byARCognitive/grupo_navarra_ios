﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScreenAfterSeconds : MonoBehaviour {

	// Use this for initialization
	void Start () {
	

		StartCoroutine (NextScene ());

	}

	private IEnumerator NextScene(){

		yield return new WaitForSeconds (.5f);

		SceneManager.LoadScene ("SelectIndustrialArquitectSceneV2");

	}
	 
}
