﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class StopTime : MonoBehaviour {

    public static bool timeIsStopped;
    private Image myImage;
    private SpriteState sprState;
    private Button myButton;

    public Sprite timeIsStoppedSprite;
    public Sprite timeIsStoppedSpritePressed;
    public Sprite timeIsNotStoppedSprite;
    public Sprite timeIsNotStoppedSpritePressed;


    void Start() {

        myButton = gameObject.GetComponent<Button>();
        sprState = new SpriteState();
        myImage = gameObject.GetComponent<Image>();
       
        EventManager.Instance.AddListener(EVENT_TYPE.RESET_STOP_TIME_BUTTON , OnEvent);


    }

    private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
        switch (Event_Type) {
            case EVENT_TYPE.RESET_STOP_TIME_BUTTON:
                Reset();
                break;
        }


    }

    public void OnClick() {

        TimeStopImage();
         
    }


    private void Reset() {

        timeIsStopped = false;
        Time.timeScale = 1;
        myButton.gameObject.GetComponent<Image>().sprite = timeIsNotStoppedSprite;         
        sprState.pressedSprite = timeIsNotStoppedSpritePressed;
        myButton.spriteState = sprState;
    }

    private void TimeStopImage() {
        if (timeIsStopped) {

            myButton.gameObject.GetComponent<Image>().sprite = timeIsNotStoppedSprite;
            sprState.pressedSprite = timeIsNotStoppedSpritePressed;
            myButton.spriteState = sprState;
            Time.timeScale = 1;

        } else {

            myButton.gameObject.GetComponent<Image>().sprite = timeIsStoppedSprite;
            sprState.pressedSprite = timeIsStoppedSpritePressed;
            myButton.spriteState = sprState;
            Time.timeScale = 0;
        }

        timeIsStopped = !timeIsStopped;
    }

}
