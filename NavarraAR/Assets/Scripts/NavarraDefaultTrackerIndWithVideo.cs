﻿

using System;
using System.Collections;
using UnityEngine;

namespace Vuforia {
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class NavarraDefaultTrackerIndWithVideo : MonoBehaviour,
                                                ITrackableEventHandler {
        #region PRIVATE_MEMBER_VARIABLES

        private TrackableBehaviour mTrackableBehaviour;


        #endregion // PRIVATE_MEMBER_VARIABLES

        public GameObject Loading;
        public GameObject child;
        public GameObject leftPanelARQ;
        public GameObject leftPanelIND;
        public GameObject renderer;
        

        #region UNTIY_MONOBEHAVIOUR_METHODS
      
        void Awake() {
           
            // seems if I do this then OnTrackableStateChanged will only run OnTrackingLost during this scene and not as a result 
            // of this scene unloading to another one
            // stops the unable to set hint, deinitialise tracker etc and allows AR to continue restarting between scenes
            PlayerPrefs.SetInt("LoadingLevelI", 0);

        }

        void Start() {
          
            EventManager.Instance.AddListener(EVENT_TYPE.ON_TRACKING_LOST, OnEvent);


            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour) {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }




        }

        private void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param) {
            switch (Event_Type) {
                case EVENT_TYPE.ON_TRACKING_LOST:
                    OnTrackingLost();

                    StartCoroutine(FoundImageTarget());

                    break;
            }


        }

        private IEnumerator FoundImageTarget() {
            yield return new WaitForSeconds(1);
            OnTrackingFound();
        }


        #endregion // UNTIY_MONOBEHAVIOUR_METHODS


        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus) {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
                OnTrackingFound();
            } else {
                if (PlayerPrefs.GetInt("LoadingLevelI") == 0) {
                    OnTrackingLost();
                }
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS



        private void OnTrackingFound() {

            Loading.SetActive(true);


            // Activate Child object

            // if (!flag) {
            //   Debug.Log("OUT");



            //} else {

            Debug.Log("IN");

            leftPanelIND.SetActive(true);
            leftPanelARQ.SetActive(false);


            EventManager.Instance.PostNotification(EVENT_TYPE.RESET_STOP_TIME_BUTTON, this);

            StartCoroutine(Load3DModel());


        }

        private IEnumerator Load3DModel() {

            yield return new WaitForSeconds(1f);

            child.SetActive(true);
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);


            //Start Loading Animation


            // Enable rendering:
            foreach (Renderer component in rendererComponents) {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents) {
                component.enabled = true;
            }

            EventManager.Instance.PostNotification(EVENT_TYPE.ZWRITE, this);
            //End Loading Animation

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

            EventManager.Instance.PostNotification(EVENT_TYPE.ZWRITE, this);
            Loading.SetActive(false);

            StartCoroutine(WaitToInitialize());

        }

        private IEnumerator WaitToInitialize() {

       

            yield return new WaitForSeconds(.01f);
            //   EventManager.Instance.PostNotification(EVENT_TYPE.UPDATE_BUTTONS_INFO, this);
            EventManager.Instance.PostNotification(EVENT_TYPE.FADE_ANIM, this);
        }

        private void OnTrackingLost() {

            Time.timeScale = 1;
            StopTime.timeIsStopped = false;

            child.SetActive(false);
            leftPanelIND.SetActive(false);

            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents) {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents) {
                component.enabled = false;
            }


            // Deactivate Child object


            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
        }



        #endregion // PRIVATE_METHODS
    }


}


